﻿function setActiveMenu(value) {
    sessionStorage.setItem('active-menu', value);
}
function getActiveMenu() {
    return sessionStorage.getItem('active-menu');
}
function applyMenu() {
    if (getActiveMenu() == null)
        resetMenu();
    else if (getActiveMenu() === 'master')
        showMasterMenu();
    else
        showTransactionMenu();
}
function resetMenu() {
    sessionStorage.clear('active-menu');
    $('.master-menu').css('display', 'block');
    $('.master-menu-child').css('display', 'none');
    $('.transaction-menu').css('display', 'block');
    $('.transaction-menu-child').css('display', 'none');
}
function showTransactionMenu() {
    $('.master-menu').css('display', 'none');
    $('.master-menu-child').css('display', 'none');
    $('.transaction-menu').css('display', 'block');
    $('.transaction-menu-child').css('display', 'block');
}
function showMasterMenu() {
    $('.master-menu').css('display', 'block');
    $('.master-menu-child').css('display', 'block');
    $('.transaction-menu').css('display', 'none');
    $('.transaction-menu-child').css('display', 'none');
}

function showListParentMenu(dom) {
    console.log(dom);
    console.log(dom.id);
    $('.list-master').toggle();
}

applyMenu();