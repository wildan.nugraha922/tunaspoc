﻿using System;
using System.Collections.Generic;

namespace Iglo.World.Api.Models
{
    public partial class RpaArheader
    {
        public RpaArheader()
        {
            RpaArdetail = new HashSet<RpaArdetail>();
        }

        public string InvoiceNum { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string HeaderDescription { get; set; }
        public string CustId { get; set; }
        public string CurrencyId { get; set; }
        public string TermsId { get; set; }

        public virtual ICollection<RpaArdetail> RpaArdetail { get; set; }
    }
}
