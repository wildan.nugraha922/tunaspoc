﻿using Iglo.World.DataAccess.Models;
using Iglo.World.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Iglo.World.Providers
{
    public interface IRoleService : IDataService<Role>
    {
       
    }
    public class RoleProvider : IRoleService
    {
        readonly TunasPOCContext context;
        public RoleProvider(TunasPOCContext context)
        {
            this.context = context;
        }
        public int Add(Role entity, string currentUser)
        {
            entity.IsActive = true;
            context.SbAdd(entity, currentUser);
            context.SaveChanges();
            return entity.Id;
        }

        public void Delete(int Id)
        {
            var data = Get(Id);
            if (data != null)
            {
                data.IsActive = false;
                context.SbDelete(data);
                context.SaveChanges();
            }
        }

        public int Edit(Role entity, string currentUser)
        {
            var data = Get(entity.Id);
            if (data != null)
            {
                data.Name = entity.Name;
                context.SbEdit(data, currentUser);
                return context.SaveChanges();
            }
            return 0;
        }

        public IQueryable<Role> Get()
        {
            return context.Role.Where(m => m.IsActive.Equals(true));
        }

        public Role Get(int id)
        {
            return Get().FirstOrDefault(x => x.Id.Equals(id));
        }
    }
}