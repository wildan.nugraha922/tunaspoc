﻿using Iglo.World.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Iglo.World.Providers
{
    public interface IAPInvoiceProvider
    {
        void Add(Apinvoice model);
    }
    public class APInvoiceProvider : IAPInvoiceProvider
    {
        readonly TunasPOCContext context;
        public APInvoiceProvider(TunasPOCContext context)
        {
            this.context = context;
        }
        public void Add(Apinvoice model)
        {
            this.context.Apinvoice.Add(model);
            this.context.SaveChanges();
        }
    }
}
