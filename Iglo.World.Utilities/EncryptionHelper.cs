﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Iglo.World.Utilities
{
    public class EncryptionHelper
    {

        public static string EncryptUrlParam(string urlParam)
        {
            string key = "jdsg432387#";

            byte[] IV = { 55, 34, 87, 64, 87, 195, 54, 21 };
            byte[] EncryptKey = Encoding.UTF8.GetBytes(key.Substring(0, 8));

            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            byte[] inputByte = Encoding.UTF8.GetBytes(urlParam);
            MemoryStream mStream = new MemoryStream();
            CryptoStream cStream = new CryptoStream(mStream, des.CreateEncryptor(EncryptKey, IV), CryptoStreamMode.Write);
            cStream.Write(inputByte, 0, inputByte.Length);
            cStream.FlushFinalBlock();
            var base64 = Convert.ToBase64String(mStream.ToArray()).Replace("+", "0pXPl5").Replace("/", "8rYj3i").Replace("=", "12Y13e");
            return base64;
        }

        public static string Encrypt(string clearText, string password)
        {
            byte[] clearBytes =
                Encoding.Unicode.GetBytes(clearText);

            PasswordDeriveBytes pdb = new PasswordDeriveBytes(password,
                                                              new byte[]
                                                                  {
                                                                      0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d,
                                                                      0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76
                                                                  });

            byte[] encryptedData = Encrypt(clearBytes,
                                           pdb.GetBytes(32), pdb.GetBytes(16));

            return Convert.ToBase64String(encryptedData);
        }

        public static string Decrypt(string cipherText, string password)
        {
            byte[] cipherBytes = Convert.FromBase64String(cipherText);

            PasswordDeriveBytes pdb = new PasswordDeriveBytes(password,
                new byte[]{
                    0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65,
                    0x64, 0x76, 0x65, 0x64, 0x65, 0x76
                });

            byte[] decryptedData = Decrypt(cipherBytes, pdb.GetBytes(32), pdb.GetBytes(16));

            return Encoding.Unicode.GetString(decryptedData);
        }

        public static byte[] Encrypt(byte[] clearData, byte[] key, byte[] iv)
        {
            MemoryStream ms = new MemoryStream();

            Rijndael alg = Rijndael.Create();

            alg.Key = key;
            alg.IV = iv;

            var cs = new CryptoStream(ms, alg.CreateEncryptor(), CryptoStreamMode.Write);

            cs.Write(clearData, 0, clearData.Length);

            cs.Close();

            byte[] encryptedData = ms.ToArray();

            return encryptedData;
        }

        public static byte[] Decrypt(byte[] cipherData, byte[] key, byte[] iv)
        {
            MemoryStream ms = new MemoryStream();

            Rijndael alg = Rijndael.Create();

            alg.Key = key;
            alg.IV = iv;

            CryptoStream cs = new CryptoStream(ms, alg.CreateDecryptor(), CryptoStreamMode.Write);

            cs.Write(cipherData, 0, cipherData.Length);

            cs.Close();

            byte[] decryptedData = ms.ToArray();

            return decryptedData;
        }

        public static string DecryptUrlParam(string urlParam)
        {
            urlParam = urlParam.Replace("0pXPl5", "+").Replace("8rYj3i", "/").Replace("12Y13e", "=");
            string key = "jdsg432387#";

            byte[] IV = { 55, 34, 87, 64, 87, 195, 54, 21 };
            byte[] inputByte = new byte[urlParam.Length];

            byte[] DecryptKey = Encoding.UTF8.GetBytes(key.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            inputByte = Convert.FromBase64String(urlParam);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(DecryptKey, IV), CryptoStreamMode.Write);
            cs.Write(inputByte, 0, inputByte.Length);
            cs.FlushFinalBlock();
            Encoding encoding = Encoding.UTF8;
            return encoding.GetString(ms.ToArray());
        }

        public static bool IsEncrypted(string text)
        {
            if (string.IsNullOrEmpty(text)) return false;
            return text.StartsWith("!=!enc!=!", StringComparison.InvariantCulture);
        }

        public static bool isFromBase64String(string input)
        {
            try
            {
                DecryptUrlParam(input);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
    }
}
