﻿using Iglo.World.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Iglo.World.Providers
{
    public interface IArTDMSDetailService : IDataService<RpaArdetail>
    {
        IQueryable<RpaArdetail> Get(string invoiceNum);
    }
    public class ARTDMSDetailProvider : IArTDMSDetailService
    {
        readonly TunasPOCContext context;
        public ARTDMSDetailProvider(TunasPOCContext context)
        {
            this.context = context;
        }
        public int Add(RpaArdetail entity, string currentUser)
        {
            context.SbAdd(entity, currentUser);
            context.SaveChanges();
            return entity.ArdetailId;
        }

        public void Delete(int Id)
        {
            var data = Get(Id);
            if (data != null)
            {
                context.Remove(data);
                context.SaveChanges();
            }
        }

        public int Edit(RpaArdetail entity, string currentUser)
        {
            throw new NotImplementedException();
        }

        public IQueryable<RpaArdetail> Get()
        {
            return context.RpaArdetail;
        }

        public RpaArdetail Get(int id)
        {
            return Get().FirstOrDefault(x => x.ArdetailId.Equals(id));
        }

        public IQueryable<RpaArdetail> Get(string invoiceNum)
        {
            return Get().Where(m => m.InvoiceNum == invoiceNum);
        }
    }
}
