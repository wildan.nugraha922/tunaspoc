﻿using System.Linq;
using System.Text.RegularExpressions;

namespace Iglo.World.Utilities
{
    public static class PasswordValidatorHelper
    {
        public static bool IsPasswordValid(string input)
        {
            var status = false;
            if (input == null)
            {
                return status;
            }
            if (input.Length < 8)
            {
                return status;
            }
            Regex regex = new Regex(@"[!@#$%^&*(),.?;:{}|<>]");
            Match match = regex.Match(input);
            if (match.Success)
                status = true;
            else
                status = false;
            if (input.Any(char.IsUpper) && input.Any(char.IsLower) && input.Any(char.IsNumber))
                status = true;
            else
                status = false;
            return status;
        }
    }

    public static class ValidationHelper
    {
        public static bool IsEmailValid(string email) {
            return Regex.IsMatch(email, @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*" + "@" + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$");
        }

        public static bool IsPhoneNumberValid(string phoneNumber)
        {
            if (phoneNumber.Length < 10 || phoneNumber.Length > 13)
                return false;
            return Regex.IsMatch(phoneNumber,@"^(08).+$");
        }        

        public static bool IsContainsSpecialCharacters(string input) {
            //var regex = new Regex(@"^[0-9a-zA-Z''-'\s]{1,40}$");
            //var match = regex.Match(input);
            //return !match.Success;
            return !Regex.IsMatch(input, @"^[0-9a-zA-Z''-'\s]{1,40}$");
        }

        public static bool IsRequired(string input) {
            var status = true;
            if (string.IsNullOrEmpty(input) || string.IsNullOrWhiteSpace(input))
                status = false;
            return status;
        }

    }
}
