﻿var arrName = [];

function onSelect(e) {
    var files = e.files;
    var acceptedFiles = [".jpg",".jpeg",".png"];
    var isAcceptedFormat = ($.inArray(files[0].extension.toLowerCase(), acceptedFiles)) != -1;

    if (!isAcceptedFormat) {
        e.preventDefault();
        swal("Warning","Files Must Be image format jpg, jpeg or png","error");
    }
}

function onSuccessUpload(e) {
    
    let attachmentId = e.response.AttachmentId;
    $('#AttachmentID').val(attachmentId);
}

$(function () {
    if ($("#AttachmentFile").val() === "") {
        $("#download-att").hide();

    } else {
        $("#download-att").show();
        var path = upload.downloadUrl + "?fileName=" + $("#AttachmentFile").val();
        $("#download-att").attr("href", path);
    }
});