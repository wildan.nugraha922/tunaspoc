﻿using Iglo.World.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Iglo.World.Providers
{
    public interface IArHeaderService : IDataService<Arheader>
    {
        string Add(Arheader entity, string currentUser);
        string Edit(Arheader entity, string currentUser);
        void Delete(string invoiceNum);
        Arheader Get(string invoiceNum);
        Arheader GetDataByInvoiceNumber(string invoiceNumber);
        void AddInvoice(Arinvoice model);
        bool IsDataExistInInvoice(string invoiceNumber);
        int CountRPAHEader();
    }
    public class ARHeaderProvider : IArHeaderService
    {
        readonly TunasPOCContext context;
        public ARHeaderProvider(TunasPOCContext context)
        {
            this.context = context;
        }

        int IDataService<Arheader>.Add(Arheader entity, string currentUser)
        {
            throw new NotImplementedException();
        }

        public string Add(Arheader entity, string currentUser)
        {
            context.SbAdd(entity, currentUser);
            context.SaveChanges();
            return entity.InvoiceNum;
        }

        public void Delete(int Id)
        {
            throw new NotImplementedException();
        }

        public void Delete(string invoiceNum)
        {
            var data = Get(invoiceNum);
            if (data != null)
            {
                context.Remove(data);
                //context.SbDelete(data);
                context.SaveChanges();
            }
        }

        public int Edit(Arheader entity, string currentUser)
        {
            throw new NotImplementedException();
        }

        string IArHeaderService.Edit(Arheader entity, string currentUser)
        {
            var data = Get(entity.InvoiceNum);
            if (data != null)
            {
                context.SbEdit(data, currentUser);
                context.SaveChanges();
            }
            return entity.InvoiceNum;
        }

        public IQueryable<Arheader> Get()
        {
            return context.Arheader;
        }

        public Arheader Get(int id)
        {
            throw new NotImplementedException();
        }

        public Arheader Get(string invoiceNum)
        {
            return context.Arheader.FirstOrDefault(x => x.InvoiceNum.Equals(invoiceNum));
        }

        public Arheader GetDataByInvoiceNumber(string invoiceNumber)
        {
            return context.Arheader.FirstOrDefault(x => x.InvoiceNum.Equals(invoiceNumber, StringComparison.OrdinalIgnoreCase));

        }

        public void AddInvoice(Arinvoice model)
        {
            this.context.Arinvoice.Add(model);
            this.context.SaveChanges();
        }

        public bool IsDataExistInInvoice(string invoiceNumber)
        {
            return this.context.Arinvoice.Where(x => x.InvoiceNum.Equals(invoiceNumber, StringComparison.OrdinalIgnoreCase)).Any();
        }

        public int CountRPAHEader()
        {
            return this.context.RpaArheader.Count();
        }
    }
}
