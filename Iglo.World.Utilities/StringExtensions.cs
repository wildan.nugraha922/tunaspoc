﻿using System;

namespace Iglo.World.Utilities
{
    public static class StringExtensions
    {
        public static string ToCurrency(this string str)
        {
            if (str == "")
                return "";

            try
            {
                return String.Format("{0:n0}", Convert.ToInt32(str));
            }
            catch (Exception)
            {
                return "";
            }
            

        }
    }
}
