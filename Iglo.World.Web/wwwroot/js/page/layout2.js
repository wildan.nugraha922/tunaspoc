﻿function onInit() {
    applyMenu();
}

function getAllMenu() {
    let menu = [
        {
            id: 1,
            name: "Master",
            url: "#",
            parentId: 0
        },
        {
            id: 2,
            name: "Submission",
            url: "#",
            parentId: 0
        },
        {
            id: 3,
            name: "Role",
            url: "Role",
            parentId: 1
        },
        {
            id: 4,
            name: "Role Menu",
            url: "RoleMenu",
            parentId: 1
        },
        {
            id: 5,
            name: "Review",
            url: "",
            parentId: 2
        },
        {
            id: 6,
            name: "Offering",
            url: "",
            parentId: 2
        },
    ];
    return menu;
}

function getHtmlMenu(id, name,url, type) {
    const menuType = type === 'parentMenu' ? 'parent-menu' : 'child-menu';
    return `<li class="nav-item" id="menu-wrapper-${id}">
                <a class="nav-link text-dark ${ menuType}" id="menu-${id}" href="${url}">${name}</a>
            </li>`;
}

function bindParentMenu() {

    const allMenu = getAllMenu();
    const parentMenus = allMenu.filter(x => x.parentId === 0)

    $('#parent-menu-list-wrapper').empty();

    for (let i = 0; i < parentMenus.length; i++) {
        const html = getHtmlMenu(parentMenus[i].id, parentMenus[i].name, parentMenus[i].url, 'parentMenu');
        $('#parent-menu-list-wrapper').append(html);
    }
    $('.parent-menu').on('click', function () {
        var arr = this.id.split('-');
        const parentId = arr[arr.length - 1];
        setActiveMenu(parentId);
        bindChildMenu(parentId);
    });
}

function bindChildMenu(parentId) {
    $('#childMenuWrapper').css('display', 'block');
    let allMenu = getAllMenu();

    let parentMenus = allMenu.filter(x => x.parentId === Number(parentId));
    $('#parent-menu-list-wrapper').empty();
    const html = getHtmlMenu(parentMenus[0].id, parentMenus[0].name, parentMenus[0].url, 'parentMenu');
    $('#parent-menu-list-wrapper').append(html);

    let childMenus = allMenu.filter(x => x.parentId === Number(parentId));

    $('#child-menu-list-wrapper').empty();
    for (let i = 0; i < childMenus.length; i++) {
        const html = getHtmlMenu(childMenus[i].id, childMenus[i].name, childMenus[i].url, 'childMenu');
        $('#child-menu-list-wrapper').append(html);
    }
}
function bindSelectedMenu(childId) {
    bindParentMenu();
    bindChildMenu(childId);
}
function setActiveMenu(value) {
    sessionStorage.setItem('active-menu', value);
}
function getActiveMenu() {
    return sessionStorage.getItem('active-menu');
}
function applyMenu() {
    if (getActiveMenu() === null || getActiveMenu() === '') {
        $('#childMenuWrapper').css('display', 'none');
        bindParentMenu();
    }
    else {
        const childId = getActiveMenu();
        bindSelectedMenu(childId);
    }
}
function resetMenu() {
    sessionStorage.clear('active-menu');
    $('#childMenuWrapper').css('display', 'none');
    bindParentMenu();
}
onInit();