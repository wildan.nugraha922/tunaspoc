﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Iglo.World.Utilities
{
    public static class AppConstant
    {
        public static string CryptographyKey = "IGLO2015";
        //public static string WorkflowRestURL = "http://10.100.10.32/K2Services";
        public static string WorkflowRestURL = "http://starbridgesv2.indocyber.co.id/K2Services";
        public static string HRAdmin = "anita.sembiring"; 
        public static int InviteRewardCount = 10;
        public static int NeedMoreTimeDay = 3;
        public static int ProbationPeriodDay = 90;
        #region Upload - FTP
        public static string FtpAddress = "ftp://localhost";
        public static string FtpUserName = "admin";
        public static string FtpPassword = "Indocyber.100";
        public static string[] AllowedExtensions = {".pdf", ".jpg", ".jpeg", ".png",".doc",".docx" };
        #endregion

        #region Url Shared Folder
        public static string IpAddress = "116.254.101.203";
        public static string ShareFolderAddress = "http://35.247.172.93:8086";
        public static string ShareFolderCVAddress = "http://35.247.172.93:8087";
        #endregion

        #region General Setting
        public static string MemberGetMemberReward = "MemberGetMemberReward";
        public static string JobApplicationReward = "JobApplicationReward";
        #endregion
    }
    public static class WorkflowProcess 
    {
        public const string MemberReference = @"IGLOWorld\MemberRequest\MemberReference";
    }
    public static class WorkflowName
    {
        public const string MemberReference = @"MemberReference";
    }
    public static class Folio
    {
        public const string MemberReference = "MemberReference";
    }
    public static class ApprovalAction {
        public const string Approve = "Approve";
        public const string Reject = "Approve";
        public const string GoToReviewTask = "GoToReviewTask";
        public const string GoToAssesmentTask = "GoToAssesmentTask";
    }
    public enum TransactionStatusEnum
    {
        //Draft = 1,
        Submitted = 1,
        Review = 2,
        Assessment = 3,
        Offering = 4,
        Reject = 5,
        Hired = 6,
        Close = 7,
        Failed = 8,
        NewTicket = 9,
        OnProgress = 10,
        Solved = 11
    }

    public enum TransactionStatusDropdownEnum 
    {
        //Draft = 1,
        Submitted = 1,
        Reviewed = 2,
        Assessed = 3,
        Offered = 4,
        Rejected = 5,
        Hired = 6,
        Close = 7,
        Failed = 8,
        NewTicket = 9,
        OnProgress = 10,
        Solved = 11
    }

    public enum NotificationTypeEnum 
    {
        Ticket = 1,
        Submission = 2,
        Announcement = 3,
        RewardRecommendation = 4,
        RewardInviteFriend = 5,
        NewTicket = 6,
        ReplyTicket = 7
    }

    public enum RewardTypeEnum
    {        
        MemberGetMember = 1,
        Referral = 2
    }
}
