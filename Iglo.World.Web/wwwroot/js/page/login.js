﻿var apiLoginUrl = window.apiLoginUrl,
    mainMenuUrl = window.mainMenuUrl,
    loginUrl = window.loginUrl;

function login() {
    showspinner();
    var r = {
        username: $("#txtUsername").val(),
        password: $("#txtPassword").val()
    };
    $.ajaxSetup({
        cache: !1
    }), $.ajax({
        url: apiLoginUrl,
        type: "post",
        dataType: "json",
        data: JSON.stringify(r),
        contentType: "application/json",
        success: function (e) {
            if (e.status.loginStatus) {
                createCookie("Auth", e.data.bearerToken, 1);
                localStorage.setItem('currentUser', e.data.userName);
                window.location.href = mainMenuUrl;
            } else {
                swal("Warning", "Username or Password is not correct", "error");
                hidespinner();
            }
            
        },
        error: function () {
            swal("Failed", "Error occur", "error"), hidespinner();
        }
    })
}

function createCookie(e, r, n) {
    var o = new Date;
    o.setTime(o.getTime() + 24 * n * 60 * 60 * 1e3), document.cookie = e + "=" + r + "; expires=" + o.toGMTString() + "; path=/"
}

function showPassword() {
    var x = document.getElementById("txtPassword");
    if (x.type === "password") {
        x.type = "text";
        $('#eyeIcon').removeClass('fa-eye');
        $('#eyeIcon').addClass('fa-eye-slash');
    } else {
        x.type = "password";
        $('#eyeIcon').addClassremoveClass('fa-eye');
        $('#eyeIcon').addClass('fa-eye-slash');
    }
}