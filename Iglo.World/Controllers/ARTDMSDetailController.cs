﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Iglo.World.DataAccess.Models;
using Iglo.World.Providers;
using Iglo.World.Utilities;
using Iglo.World.ViewModels;
using IgloWorld.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Iglo.World.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ARTDMSDetailController : Controller
    {
        private IArTDMSDetailService _arDetailService;
        private IArTDMSHeaderService _arHeaderService;
        private IMapper _mapper;
        private AjaxViewModel _ajaxViewModel;
        public ARTDMSDetailController(IArTDMSDetailService arDetailService, IArTDMSHeaderService arHeaderService, IMapper mapper)
        {
            _arDetailService = arDetailService;
            _arHeaderService = arHeaderService;
            _mapper = mapper;
            _ajaxViewModel = new AjaxViewModel();
        }

        [HttpPost]
        [Route("[action]/{id}")]
        public IActionResult GetList([FromBody]CustomDataSourceRequest request, string id)
        {
            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    var dataRole = _arDetailService.Get(id).ToList();
                    DataSourceRequest req = GridUtilities.ConvertToKendoFromCustomRequest(request);
                    var filter = dataRole.ToDataSourceResult(req);
                    return Ok(filter);
                }
                else
                    return Ok();
            }

            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult Submit([FromBody] ArDetailViewModel viewModel)
        {
            var currentUser = "Wildan";
            if (viewModel is null)
            {
                _ajaxViewModel.SetValues(false, null, "Data is null");
                return BadRequest(_ajaxViewModel);
            }
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            //if (string.IsNullOrEmpty(viewModel.InvoiceNum))

            var isDataExistInInvoice = _arHeaderService.IsDataExistInInvoice(viewModel.InvoiceNum);
            if (isDataExistInInvoice)
            {
                _ajaxViewModel.SetValues(false, null, "Data Already exists in invoice");
                return Ok(_ajaxViewModel);
            }

            if (viewModel.ArdetailId == 0)
            {
                var model = _mapper.Map<RpaArdetail>(viewModel);
                var dataHeader = _arHeaderService.GetDataByInvoiceNumber(model.InvoiceNum);
                var invoice = new Arinvoice
                {
                    InvoiceNum = model.InvoiceNum,
                    CurrencyCode = dataHeader.CurrencyId,
                    TermsCode = dataHeader.TermsId,
                    Description = dataHeader.HeaderDescription,
                    CustId = dataHeader.CustId,
                    InvoiceDate = DateTime.Now,
                    ApplyDate = DateTime.Now,
                    Flag = "NON RPA",
                    Synchronized = false,
                    EpicorMessage = "",
                    InvoiceLine = model.InvoiceLine.HasValue ? model.InvoiceLine.Value : 1,
                    PartNum = model.PartNum,
                    LineDesc = model.PartDescriptions,
                    Qty = model.Qty,
                    Pum = model.Uom,
                    Cost = model.Cost,
                    TotalPrice = model.TotalCost,
                    EpicorInvoiceNum = ""
                };
                //_arHeaderService.AddInvoice(invoice);
                _arDetailService.Add(model, currentUser);
            }
            else
            {
                var existingRole = _arDetailService.Get(viewModel.ArdetailId);
                _mapper.Map(viewModel, existingRole);
                _arDetailService.Edit(existingRole, currentUser);
            }
            _ajaxViewModel.SetValues(true, null, "Saved");
            return Ok(_ajaxViewModel);
        }

        [HttpPost]
        [Route("[action]/{Id}")]
        public IActionResult DeleteRow(int id)
        {

            try
            {
                var userExist = _arDetailService.Get(id);
                if (userExist != null)
                {
                    _arDetailService.Delete(id);
                    _ajaxViewModel.SetValues(true, "", "Success! Data has been deleted");
                }
                else
                {
                    _ajaxViewModel.SetValues(false, "", "Failed to Delete");
                }

            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
            return Ok(_ajaxViewModel);
        }
    }
}