﻿using System;
using System.Collections.Generic;
using Iglo.World.Utilities;
using Iglo.World.ViewModels;
using Iglo.World.Web.Filters;
using IgloWorld.ViewModels;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace Iglo.World.Web.Controllers
{
    public class RoleController : Controller
    {

        AjaxViewModel _ajaxViewModel;
        public RoleController()
        {
            _ajaxViewModel = new AjaxViewModel();
        }
        public IActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            var model = new RoleViewModel();
            return PartialView("CreateEdit", model);
        }
        [HttpPost]
        public JsonResult List([DataSourceRequest] DataSourceRequest request)
        {
            DataSourceResult newDataSourceResult = new DataSourceResult();
            try
            {
                var listRole = new List<ViewModels.RoleViewModel>();

                var json = GridUtilities.ConvertKendoRequestToJson(request);
                var endpoint = $"{ApiUrl.BaseUrl}/Role/GetList";
                var result = RestAPIHelper<CustomDataSourceResult<RoleViewModel>>.Submit(json, Method.POST, endpoint, Request);

                var dataRoles = result.data;
                newDataSourceResult.Data = dataRoles;
                newDataSourceResult.Total = result.total;

            }
            catch (Exception ex)
            {
                _ajaxViewModel.SetValues(false, null, string.Format("Failed\\nMessage: {0}", ex.GetBaseException().Message));
            }

            return Json(newDataSourceResult);
        }
        public ActionResult Edit(int id)
        {
            var model = new RoleViewModel();
            try
            {
                var endpoint = $"{ApiUrl.BaseUrl}/Role/Get/{id}";
                model = RestAPIHelper<RoleViewModel>.Submit("", Method.GET, endpoint, Request);
            }
            catch (Exception ex)
            {
                _ajaxViewModel.SetValues(false, null, "");
            }
            return PartialView("CreateEdit", model);
        }
        [HttpPost]
        public ActionResult Submit(RoleViewModel model)
        {

            var endpoint = $"{ApiUrl.BaseUrl}/Role/Submit";
            try
            {
                string json = JsonConvert.SerializeObject(model);

                var isValid = RestAPIHelper<AjaxViewModel>.Submit(json, Method.POST, endpoint, Request);
                if (!isValid.IsSuccess)
                {
                    _ajaxViewModel.SetValues(isValid.IsSuccess, null, isValid.Message);
                }
                else
                {
                    _ajaxViewModel.SetValues(true, null, "Saved");
                }
            }
            catch (Exception ex)
            {
                _ajaxViewModel.SetValues(false, null, "Failed Message : " + ex.GetBaseException().Message);
            }
            return Json(_ajaxViewModel);
        }
        [HttpPost]
        public IActionResult DeleteRow(string id)
        {
            try
            {
                var endpoint = $"{ApiUrl.BaseUrl}/Role/DeleteRow/{id}";
                var delete = RestAPIHelper<AjaxViewModel>.Submit("", Method.POST, endpoint, Request);
                _ajaxViewModel.SetValues(delete.IsSuccess, null, delete.Message);
            }
            catch (Exception)
            {
                _ajaxViewModel.SetValues(false, null, "Failed Message : ");
            }
            return Json(_ajaxViewModel);
        }
    }
}