﻿using System;
using System.Collections.Generic;

namespace Iglo.World.DataAccess.Models
{
    public partial class Arheader
    {
        public Arheader()
        {
            Ardetail = new HashSet<Ardetail>();
        }

        public string InvoiceNum { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string HeaderDescription { get; set; }
        public string CustId { get; set; }
        public string CurrencyId { get; set; }
        public string TermsId { get; set; }

        public virtual ICollection<Ardetail> Ardetail { get; set; }
    }
}
