﻿using Iglo.World.Providers;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iglo.World.Api
{
    public static class ServiceRegister
    {
        public static void RegisterPhoenixDependencies(this IServiceCollection services)
        {
            services.AddScoped<IRoleService, RoleProvider>();           
            services.AddScoped<IAPService, APProvider>();
            services.AddScoped<IAPTDMSService, APTDMSProvider>();
            services.AddScoped<IArHeaderService, ARHeaderProvider>();
            services.AddScoped<IArTDMSHeaderService, ARTDMSHeaderProvider>();
            services.AddScoped<IArTDMSDetailService, ARTDMSDetailProvider>();
            services.AddScoped<IArDetailService, ARDetailProvider>();
        }
    }
}
