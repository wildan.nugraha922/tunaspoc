﻿var arrName = [];

function onSelect(e) {
    var files = e.files;
    var acceptedFiles = [".doc",".docx",".pdf"];
    var isAcceptedFormat = ($.inArray(files[0].extension, acceptedFiles)) != -1;

    if (!isAcceptedFormat) {
        e.preventDefault();
        swal("Warning","Files Must Be image format doc, docx or pdf","error");
    }
}

function onSuccessUpload(e) {
    
    let attachmentId = e.response.AttachmentId;
    $('#AttachmentId').val(attachmentId);
}

$(function () {
    if ($("#AttachmentFile").val() === "") {
        $("#download-att").hide();

    } else {
        $("#download-att").show();
        var path = upload.downloadUrl + "?fileName=" + $("#AttachmentFile").val();
        $("#download-att").attr("href", path);
    }
});