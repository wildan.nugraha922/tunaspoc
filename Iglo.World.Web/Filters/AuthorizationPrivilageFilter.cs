﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using System;

namespace Iglo.World.Web.Filters
{
    public class AuthorizationPrivilageFilter : Attribute//, IResourceFilter
    {
        public void OnResourceExecuting(ResourceExecutingContext context)
        {
            var cookie = context.HttpContext.Request.Cookies["Auth"];
            if (cookie == null)
            {
                context.Result = new RedirectToRouteResult(new RouteValueDictionary{{ "controller", "Login" },
                                        { "action", "Index" }
                                        });
            }

        }

        //public void OnResourceExecuted(ResourceExecutedContext context)
        //{
        //}
    }
}
