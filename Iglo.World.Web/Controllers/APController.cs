﻿using System;
using System.Collections.Generic;
using Iglo.World.Utilities;
using Iglo.World.ViewModels;
using Iglo.World.Web.Filters;
using IgloWorld.ViewModels;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace Iglo.World.Web.Controllers
{
    public class APController : Controller
    {

        AjaxViewModel _ajaxViewModel;
        public APController()
        {
            _ajaxViewModel = new AjaxViewModel();
        }
        public IActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            var model = new ApHeaderViewModel();
            model.InvoiceDate = DateTime.Now;
            return PartialView("CreateEdit", model);
        }
        public ActionResult CreateDetail(string invoiceNumber)
        {
            var model = new ApDetailViewModel();
            model.InvoiceNum = invoiceNumber;
            return PartialView("CreateEditDetail", model);
        }
        [HttpPost]
        public JsonResult List([DataSourceRequest] DataSourceRequest request)
        {
            DataSourceResult newDataSourceResult = new DataSourceResult();
            try
            {
                var listRole = new List<ViewModels.ApHeaderViewModel>();

                var json = GridUtilities.ConvertKendoRequestToJson(request);
                var endpoint = $"{ApiUrl.BaseUrl}/AP/GetList";
                var result = RestAPIHelper<CustomDataSourceResult<ApHeaderViewModel>>.Submit(json, Method.POST, endpoint, Request);

                var dataRoles = result.data;
                newDataSourceResult.Data = dataRoles;
                newDataSourceResult.Total = result.total;

            }
            catch (Exception ex)
            {
                _ajaxViewModel.SetValues(false, null, string.Format("Failed\\nMessage: {0}", ex.GetBaseException().Message));
            }

            return Json(newDataSourceResult);
        }

        [HttpPost]
        public JsonResult ListDetail([DataSourceRequest] DataSourceRequest request, string invoiceNumber)
        {
            DataSourceResult newDataSourceResult = new DataSourceResult();
            try
            {
                var listRole = new List<ViewModels.ApDetailViewModel>();

                var json = GridUtilities.ConvertKendoRequestToJson(request);
                var endpoint = $"{ApiUrl.BaseUrl}/AP/GetListDetail?invoiceNumber={invoiceNumber}";
                var result = RestAPIHelper<CustomDataSourceResult<ApDetailViewModel>>.Submit(json, Method.POST, endpoint, Request);

                var dataRoles = result.data;
                newDataSourceResult.Data = dataRoles;
                newDataSourceResult.Total = result.total;

            }
            catch (Exception ex)
            {
                _ajaxViewModel.SetValues(false, null, string.Format("Failed\\nMessage: {0}", ex.GetBaseException().Message));
            }

            return Json(newDataSourceResult);
        }


        public ActionResult Edit(int id)
        {
            var model = new ApHeaderViewModel();
            try
            {
                var endpoint = $"{ApiUrl.BaseUrl}/AP/Get/{id}";
                model = RestAPIHelper<ApHeaderViewModel>.Submit("", Method.GET, endpoint, Request);
            }
            catch (Exception ex)
            {
                _ajaxViewModel.SetValues(false, null, "");
            }
            return PartialView("CreateEdit", model);
        }
        [HttpPost]
        public ActionResult Submit(ApHeaderViewModel model)
        {

            var endpoint = $"{ApiUrl.BaseUrl}/AP/Submit";
            try
            {
                string json = JsonConvert.SerializeObject(model);

                var isValid = RestAPIHelper<AjaxViewModel>.Submit(json, Method.POST, endpoint, Request);
                if (!isValid.IsSuccess)
                {
                    _ajaxViewModel.SetValues(isValid.IsSuccess, null, isValid.Message);
                }
                else
                {
                    _ajaxViewModel.SetValues(true, null, "Saved");
                }
            }
            catch (Exception ex)
            {
                _ajaxViewModel.SetValues(false, null, "Failed Message : " + ex.GetBaseException().Message);
            }
            return Json(_ajaxViewModel);
        }

        [HttpPost]
        public ActionResult SubmitDetail(ApDetailViewModel model)
        {

            var endpoint = $"{ApiUrl.BaseUrl}/AP/SubmitDetail";
            try
            {
                model.InvoiceLine = 1;
                string json = JsonConvert.SerializeObject(model);

                var isValid = RestAPIHelper<AjaxViewModel>.Submit(json, Method.POST, endpoint, Request);
                if (!isValid.IsSuccess)
                {
                    _ajaxViewModel.SetValues(isValid.IsSuccess, null, isValid.Message);
                }
                else
                {
                    _ajaxViewModel.SetValues(true, null, "Saved");
                }
            }
            catch (Exception ex)
            {
                _ajaxViewModel.SetValues(false, null, "Failed Message : " + ex.GetBaseException().Message);
            }
            return Json(_ajaxViewModel);
        }

        [HttpPost]
        public IActionResult DeleteRow(string id)
        {
            try
            {
                var endpoint = $"{ApiUrl.BaseUrl}/AP/DeleteRow/{id}";
                var delete = RestAPIHelper<AjaxViewModel>.Submit("", Method.POST, endpoint, Request);
                _ajaxViewModel.SetValues(delete.IsSuccess, null, delete.Message);
            }
            catch (Exception)
            {
                _ajaxViewModel.SetValues(false, null, "Failed Message : ");
            }
            return Json(_ajaxViewModel);
        }

        [HttpGet]
        public IActionResult GetInvoiceNumber()
        {
            try
            {
                var endpoint = $"{ApiUrl.BaseUrl}/AP/GetInvoiceNumber";
                var data = RestAPIHelper<AjaxViewModel>.Submit("", Method.GET, endpoint, Request);
                _ajaxViewModel.SetValues(data.IsSuccess, data.Data, data.Message);
            }
            catch (Exception)
            {
                _ajaxViewModel.SetValues(false, null, "Failed Message : ");
            }
            return Json(_ajaxViewModel);
        }
    }
}