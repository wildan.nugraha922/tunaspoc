﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Iglo.World.ViewModels
{
    public partial class ArheaderViewModel
    {
        public ArheaderViewModel()
        {
            Ardetail = new HashSet<ArDetailViewModel>();
        }
        [StringLength(50, ErrorMessage = "Maximum 50 characters")]
        public string InvoiceNum { get; set; }
        public DateTime InvoiceDate { get; set; }
        [StringLength(30, ErrorMessage = "Maximum 30 characters")]
        public string HeaderDescription { get; set; }
        [StringLength(50, ErrorMessage = "Maximum 50 characters")]
        public string CustId { get; set; }
        [StringLength(4, ErrorMessage = "Maximum 4 characters")]
        public string CurrencyId { get; set; }
        [StringLength(4, ErrorMessage = "Maximum 4 characters")]
        public string TermsId { get; set; }

        public virtual ICollection<ArDetailViewModel> Ardetail { get; set; }
    }
}
