﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Iglo.World.ViewModels
{
    public class ApHeaderViewModel
    {
         public ApHeaderViewModel()
        {
            Apdetail = new HashSet<ApDetailViewModel>();
        }
        [StringLength(50, ErrorMessage = "Maximum 50 characters")]
        public string InvoiceNum { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string InvoiceDateString { get; set; }
        [StringLength(30, ErrorMessage = "Maximum 30 characters")]
        public string HeaderDescription { get; set; }
        [StringLength(50, ErrorMessage = "Maximum 50 characters")]
        public string VendorId { get; set; }
        [StringLength(4, ErrorMessage = "Maximum 4 characters")]
        public string CurrencyId { get; set; }
        [StringLength(4, ErrorMessage = "Maximum 4 characters")]
        public string TermsId { get; set; }
        public virtual ICollection<ApDetailViewModel> Apdetail { get; set; }
    }
}
