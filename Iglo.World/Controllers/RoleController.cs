﻿using System;
using System.Linq;
using AutoMapper;
using Iglo.World.DataAccess.Models;
using Iglo.World.Providers;
using Iglo.World.Utilities;
using Iglo.World.ViewModels;
using IgloWorld.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;

namespace Iglo.World.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : Controller
    {
        private IRoleService _roleService;
        private IMapper _mapper;
        private AjaxViewModel _ajaxViewModel;
        public RoleController(IRoleService roleService, IMapper mapper)
        {
            _roleService = roleService;
            _mapper = mapper;
            _ajaxViewModel = new AjaxViewModel();
        }

        [HttpGet]
        [Route("[action]/{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var role = _roleService.Get(id);
                var vm = _mapper.Map<RoleViewModel>(role);
                return Ok(vm);
            }

            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult GetList([FromBody]CustomDataSourceRequest request)
        {
            try
            {
                var dataRole = _roleService.Get().ToList();
                DataSourceRequest req = GridUtilities.ConvertToKendoFromCustomRequest(request);
                var filter = dataRole.ToDataSourceResult(req);
                return Ok(filter);
            }

            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult Submit([FromBody] RoleViewModel viewModel)
        {
            var currentUser = "Wildan";
            if (viewModel is null)
            {
                _ajaxViewModel.SetValues(false, null, "Data is null");
                return BadRequest(_ajaxViewModel);
            }
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (viewModel.ID == 0)
            {
                var model = _mapper.Map<Role>(viewModel);
                _roleService.Add(model, currentUser);
            }
            else
            {
                var existingRole = _roleService.Get(viewModel.ID);
                _mapper.Map(viewModel, existingRole);
                _roleService.Edit(existingRole, currentUser);
            }
            _ajaxViewModel.SetValues(true, null, "Saved");
            return Ok(_ajaxViewModel);
        }

        [HttpPost]
        [Route("[action]/{Id}")]
        public IActionResult DeleteRow(int id)
        {
            
            try
            {
                var userExist = _roleService.Get(id);
                if (userExist != null)
                {
                    _roleService.Delete(id);
                    _ajaxViewModel.SetValues(true, "", "Success! Data has been deleted");

                }
                else
                {
                    _ajaxViewModel.SetValues(false, "", "Failed to Delete");
                }

            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
            return Ok(_ajaxViewModel);
        }

        [HttpGet]
        [Route("[action]")]

        public IActionResult GetRoleDropDown()
        {
            var data = _roleService.Get();
            return Ok(data.Select(x => new { Value = x.Id, Text = x.Name }));
        }

        

    }
}