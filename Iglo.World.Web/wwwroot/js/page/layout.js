﻿function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function getAllMenuApi() {
    var dfd = jQuery.Deferred();

    let menus = sessionStorage.getItem('menu');

    if (menus === null) {
        //let menu = [];
        var e = window.menuUrl;
        var bearertoken = getCookie('Auth');
        $.ajax({
            url: e, type: "GET",
            dataType: "json",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + bearertoken
            },
            success:
                function (response) {
                    dfd.resolve(response.menu);
                    const temp = response.menu;
                    sessionStorage.setItem('menu', JSON.stringify(temp));
                },
            error: function (err) {
                dfd.reject(err);
            }
        });
    } else {
        dfd.resolve(JSON.parse(menus));
    }

    
    return dfd.promise();
}

function generateMenu(e) {
    if (null === e) deleteCookie(), window.location.href = loginUrl;
    else
        for (var n = e.length, o = 0; o < n; o++) {
            var t = e[o].ControllerName, i = (e[o].Action, "<a href='/" + t + "' class='list-group-item list-group-item-action bg-light'>" + e[o].MenuName + "</a>"); $("#divMenu").append(i)
        }
}

function deleteCookie() { createCookie("Auth", "", -1) }


function getHtmlMenu(id, name, url, type) {
    const menuType = type === 'parentMenu' ? 'parent-menu' : 'child-menu';
    return `<li class="nav-item" id="menu-wrapper-${id}">
                <a class="nav-link text-dark ${ menuType}" id="menu-${id}" href="${url}">${name}</a>
            </li>`;
}

function getHtmlSubNav(id,name) {
    return `<li class="nav-item">
                <a class="nav-link dropdown-button" onclick="showListParentMenu(this)" href="#" id="${id}" style="font-weight:bolder !important">${name}<span class="fa fa-angle-down custom-arrow"></span> </a>
                <div class="list-master" style="display:none">
                    <ul id='list-master-wrapper'>
                        
                    </ul>
                </div>
            </li>`;
}

function getHtmlListSubNav(url, name) {
    return `<li>
                <a href="${url}">${name}</a>
            </li>`;
}

function setActiveMenu(value) {
    sessionStorage.setItem('active-menu', value);
}

function clearActiveMenu() {
    sessionStorage.setItem('active-menu', null)
}

function getActiveMenu() {
    return JSON.parse(sessionStorage.getItem('active-menu'));
}

function applyMenu() {

    checkCurrentMenu().then(function () {
        const activeMenu = getActiveMenu();
        if (activeMenu === null || activeMenu === '')
            showParentMenu();
        else {
            showSelectedMenu(activeMenu);
        }
    });
    
}

function showParentMenu() {
    $('#main-nav').show();

    getAllMenuApi().then(function (response) {
        let allMenu = response;
        const parentMenus = allMenu.filter(x => x.parentId === 0);
        $('#main-nav').empty();

        for (let i = 0; i < parentMenus.length; i++) {
            const html = getHtmlMenu(parentMenus[i].id, parentMenus[i].name, parentMenus[i].url, 'parentMenu');
            $('#main-nav').append(html);
        }
        $('.parent-menu').on('click', function () {
            var arr = this.id.split('-');
            const parentId = arr[arr.length - 1];
            setActiveMenu(parentId);
            showChildMenu(parentId);
        });


    }).catch(function (err) {
        console.log('terjadi kesalahan ', err);
    });

}

function showChildMenu(parentId) {

    $('#sub-nav').empty();

    getAllMenuApi().then(function (response) {
        let allMenu = response;
        let activeNavMenu = allMenu.filter(x => x.parentId === Number(parentId));
        let subNavHtml = getHtmlSubNav(activeNavMenu[0].id, activeNavMenu[0].name);
        $('#sub-nav').append(subNavHtml);


        let childMenus = allMenu.filter(x => x.parentId === Number(parentId));

        for (let i = 0; i < childMenus.length; i++) {
            const html = getHtmlMenu(childMenus[i].id, childMenus[i].name, childMenus[i].url, 'childMenu');
            $('#sub-nav').append(html);
        }
        $('#main-nav').hide();
        $('#sub-nav').show();
    }).catch(function (err) {
        console.log('terjadi kesalahan ', err);
    });
    
}

function showSelectedMenu(childId) {
    

    getAllMenuApi().then(function (resopnse) {
        const allMenu = resopnse;
        const currentMenu = allMenu.filter(x => x.id === Number(childId));
        if (currentMenu.length > 0)
            showChildMenu(currentMenu[0].parentId);
    }).catch(function (err) {
        console.log('terjadi kesalahan ', err);
    });


}

function resetMenu() {
    sessionStorage.clear('active-menu');
}

function showListParentMenu(dom) {

    const selectedParentId = Number(dom.id);

    getAllMenuApi().then(function (response) {
        var allMenu = response.filter(x => x.parentId === 0);
        var listMenu = allMenu.filter(x => x.id !== selectedParentId);

        $('#list-master-wrapper').empty();
        for (let i = 0; i < listMenu.length; i++) {
            const html = getHtmlListSubNav(listMenu[i].url, listMenu[i].name);
            $('#list-master-wrapper').append(html);
        }

        $('.list-master').toggle();
        $('.custom-arrow').toggleClass('fa-angle-down').toggleClass('fa-angle-up');

    }).catch(function (err) {
        console.log('terjadi kesalahan ', err);
    });
}

function checkCurrentMenu() {

    var dfd = jQuery.Deferred();

    let currentUrl = document.URL;
    let arr = currentUrl.split('/');
    let lastUrl = arr[arr.length - 1];
    getAllMenuApi().then(function (response) {
        let allMenu = response;
        let currentMenu = allMenu.filter(x => x.url.toLowerCase() === lastUrl.toLowerCase() && x.parentId !== 0);
        if (currentMenu.length > 0)
            setActiveMenu(currentMenu[0].id);
        else
            clearActiveMenu();

        dfd.resolve();

    }).catch(function (err) {
        console.log('terjadi kesalahan ', err);
        dfd.reject(err);
    });

    return dfd.promise();
}

function createCookie(e, n, o) {
    if (o) {
        var t = new Date;
        t.setTime(t.getTime() + 24 * o * 60 * 60 * 1e3);
        var i = "; expires=" + t.toGMTString()
    } else i = "";
    document.cookie = e + "=" + n + i + "; path=/"
}

function clearCookie() {
    createCookie("Auth", "", -1)
}

function clearActiveUser() {
    $('#current-user').val('');
    sessionStorage.clear();
}

function onLogout() {
    clearCookie();
    clearActiveUser();
    var loginUrl = window.loginUrl;
    window.location.href = loginUrl;
}

function showCurrentUser() {
    let currentUser = localStorage.getItem('currentUser');
    $('#current-user').text(currentUser);
}

function onInit() {
    applyMenu();
    showCurrentUser();
}

onInit();

