﻿using AutoMapper;
using Iglo.World.DataAccess.Models;
using Iglo.World.ViewModels;
using System;
using System.Collections.Generic;

namespace Iglo.World.Api
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Role, RoleViewModel>();
            CreateMap<RoleViewModel, Role>();  
            
            CreateMap<Apheader, ApHeaderViewModel>();           
            CreateMap<ApHeaderViewModel, Apheader>();

            CreateMap<RpaApheader, ApHeaderViewModel>();
            CreateMap<ApHeaderViewModel, RpaApheader>();


            CreateMap<List<Apheader>, List<ApHeaderViewModel>>();
            CreateMap<List<ApHeaderViewModel>, List<Apheader>>();

            CreateMap<List<RpaApheader>, List<ApHeaderViewModel>>();
            CreateMap<List<ApHeaderViewModel>, List<RpaApheader>>();

            CreateMap<Apdetail, ApDetailViewModel>();
            CreateMap<ApDetailViewModel, Apdetail>();

            CreateMap<RpaApdetail, ApDetailViewModel>();
            CreateMap<ApDetailViewModel, RpaApdetail>();

            CreateMap<Arheader, ArheaderViewModel>();
            CreateMap<ArheaderViewModel, Arheader>();

            CreateMap<RpaArheader, ArheaderViewModel>();
            CreateMap<ArheaderViewModel, RpaArheader>();

            CreateMap<Ardetail, ArDetailViewModel>();
            CreateMap<ArDetailViewModel, Ardetail>();

            CreateMap<RpaArdetail, ArDetailViewModel>();
            CreateMap<ArDetailViewModel, RpaArdetail>();
        }
        public class DateTimeConverterForString : ITypeConverter<string, DateTime?>
        {
            public DateTime? Convert(string source, DateTime? destination, ResolutionContext context)
            {
                if (string.IsNullOrEmpty(source))
                    return null;
                else
                {
                    if (source.Length <= 6)
                        return new DateTime(System.Convert.ToInt32(source.Substring(0, 4)), System.Convert.ToInt32(source.Substring(4, 2)), 1);
                    else if (source.Length <= 8)
                        return new DateTime(System.Convert.ToInt32(source.Substring(0, 4)), System.Convert.ToInt32(source.Substring(4, 2)), System.Convert.ToInt32(source.Substring(6, 2)));
                    else
                        return new DateTime(System.Convert.ToInt32(source.Substring(0, 4)), System.Convert.ToInt32(source.Substring(4, 2)), System.Convert.ToInt32(source.Substring(6, 2)), System.Convert.ToInt32(source.Substring(8, 2)), System.Convert.ToInt32(source.Substring(10, 2)), System.Convert.ToInt32(source.Substring(12, 2)));
                }
            }
        }
    }
}
