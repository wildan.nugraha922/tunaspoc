﻿var changePasswordUrl = window.changePasswordUrl;

onInit();

function onInit() {
    var param = getParameterByName('RequestId');
    if (param === null)
        param = getParameterByName('Requestid')
    if (param === null)
        param = getParameterByName('requestid')

    if (param === null) {
        swal("Warning", "Link is not valid", "error");
        window.location.href = 'Account/ForgotPasswordExpired'
        return;
    }

    setHiddenValue(param);
}
function setHiddenValue(requestId) {
    $('#hidRequestId').val(requestId);
}
function getParameterByName(name,url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function changePassword() {
    showspinner();
    var r = {
        requestId: $("#hidRequestId").val(),
        password: $("#txtPassword").val(),
        confirmPassword: $("#txtConfirmPassword").val()
    };
    $.ajaxSetup({
        cache: !1
    }), $.ajax({
        url: '/Account/ChangePassword',
        type: "post",
        dataType: "json",
        data: JSON.stringify(r),
        contentType: "application/json",
        success: function (e) {
            if (e.IsSuccess) {
                swal("Warning", e.Message, "success");
                $("#txtPassword").val('');
                $("#txtConfirmPassword").val('');
                hidespinner();
            } else {
                swal("Warning", e.Message, "error");
                hidespinner();
            }
            
        },
        error: function () {
            swal("Failed", "Error occur", "error"), hidespinner();
        }
    })
}
function showPassword() {
    var x = document.getElementById("txtPassword");
    if (x.type === "password") {
        x.type = "text";
        $('#eyeIcon').removeClass('fa-eye');
        $('#eyeIcon').addClass('fa-eye-slash');
    } else {
        x.type = "password";
        $('#eyeIcon').addClass('fa-eye');
        $('#eyeIcon').removeClass('fa-eye-slash');
    }
}
function showConfirmPassword() {
    var x = document.getElementById("txtConfirmPassword");
    if (x.type === "password") {
        x.type = "text";
        $('#eyeIconConfirmPassword').removeClass('fa-eye');
        $('#eyeIconConfirmPassword').addClass('fa-eye-slash');
    } else {
        x.type = "password";
        $('#eyeIconConfirmPassword').addClass('fa-eye');
        $('#eyeIconConfirmPassword').removeClass('fa-eye-slash');
    }
}

