﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iglo.World.Utilities;
using Iglo.World.ViewModels;
using IgloWorld.ViewModels;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace Iglo.World.Web.Controllers
{
    public class ARController : Controller
    {
        AjaxViewModel _ajaxViewModel;
        public ARController()
        {
            _ajaxViewModel = new AjaxViewModel();
        }

        public IActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            var model = new ArheaderViewModel();
            return PartialView("CreateEdit", model);
        }

        [HttpPost]
        public ActionResult Create(ArheaderViewModel model)
        {

            var endpoint = $"{ApiUrl.BaseUrl}/AR/Submit";
            try
            {
                string json = JsonConvert.SerializeObject(model);

                var isValid = RestAPIHelper<AjaxViewModel>.Submit(json, Method.POST, endpoint, Request);
                if (!isValid.IsSuccess)
                {
                    _ajaxViewModel.SetValues(isValid.IsSuccess, null, isValid.Message);
                }
                else
                {
                    _ajaxViewModel.SetValues(true, null, isValid.Message);
                }
            }
            catch (Exception ex)
            {
                _ajaxViewModel.SetValues(false, null, "Failed Message : " + ex.GetBaseException().Message);
            }
            return Json(_ajaxViewModel);
        }

        [HttpPost]
        public ActionResult Edit(ArheaderViewModel model)
        {
            var endpoint = $"{ApiUrl.BaseUrl}/AR/Submit";
            try
            {
                string json = JsonConvert.SerializeObject(model);

                var isValid = RestAPIHelper<AjaxViewModel>.Submit(json, Method.POST, endpoint, Request);
                if (!isValid.IsSuccess)
                {
                    _ajaxViewModel.SetValues(isValid.IsSuccess, null, isValid.Message);
                }
                else
                {
                    _ajaxViewModel.SetValues(true, null, isValid.Message);
                }
            }
            catch (Exception ex)
            {
                _ajaxViewModel.SetValues(false, null, "Failed Message : " + ex.GetBaseException().Message);
            }
            return Json(_ajaxViewModel);
        }

        public ActionResult CreateDetail(string InvoiceNo)
        {
            var model = new ArDetailViewModel();
            model.InvoiceNum = InvoiceNo;
            return PartialView("CreateEditDetail", model);
        }

        [HttpPost]
        public ActionResult CreateDetail(ArDetailViewModel model)
        {

            var endpoint = $"{ApiUrl.BaseUrl}/ARDetail/Submit";
            try
            {
                model.InvoiceLine = 1;
                string json = JsonConvert.SerializeObject(model);

                var isValid = RestAPIHelper<AjaxViewModel>.Submit(json, Method.POST, endpoint, Request);
                if (!isValid.IsSuccess)
                {
                    _ajaxViewModel.SetValues(isValid.IsSuccess, null, isValid.Message);
                }
                else
                {
                    _ajaxViewModel.SetValues(true, null, isValid.Message);
                }
            }
            catch (Exception ex)
            {
                _ajaxViewModel.SetValues(false, null, "Failed Message : " + ex.GetBaseException().Message);
            }
            return Json(_ajaxViewModel);
        }

        [HttpPost]
        public JsonResult List([DataSourceRequest] DataSourceRequest request)
        {
            DataSourceResult newDataSourceResult = new DataSourceResult();
            try
            {
                var listAr = new List<ViewModels.ArheaderViewModel>();

                var json = GridUtilities.ConvertKendoRequestToJson(request);
                var endpoint = $"{ApiUrl.BaseUrl}/AR/GetList";
                var result = RestAPIHelper<CustomDataSourceResult<ArheaderViewModel>>.Submit(json, Method.POST, endpoint, Request);

                var dataAR = result.data;
                newDataSourceResult.Data = dataAR;
                newDataSourceResult.Total = result.total;

            }
            catch (Exception ex)
            {
                _ajaxViewModel.SetValues(false, null, string.Format("Failed\\nMessage: {0}", ex.GetBaseException().Message));
            }

            return Json(newDataSourceResult);
        }

        [HttpPost]
        public JsonResult ListDetail([DataSourceRequest] DataSourceRequest request, string headerID)
        {
            DataSourceResult newDataSourceResult = new DataSourceResult();
            try
            {
                var listRole = new List<ViewModels.ArDetailViewModel>();

                var json = GridUtilities.ConvertKendoRequestToJson(request);
                var endpoint = $"{ApiUrl.BaseUrl}/ARDetail/GetList/"+headerID;
                var result = RestAPIHelper<CustomDataSourceResult<ArDetailViewModel>>.Submit(json, Method.POST, endpoint, Request);

                var dataRoles = result.data;
                newDataSourceResult.Data = dataRoles;
                newDataSourceResult.Total = result.total;

            }
            catch (Exception ex)
            {
                _ajaxViewModel.SetValues(false, null, string.Format("Failed\\nMessage: {0}", ex.GetBaseException().Message));
            }

            return Json(newDataSourceResult);
        }

        [HttpPost]
        public IActionResult Delete(string id)
        {
            try
            {
                var endpoint = $"{ApiUrl.BaseUrl}/AR/DeleteRow/{id}";
                var delete = RestAPIHelper<AjaxViewModel>.Submit("", Method.POST, endpoint, Request);
                _ajaxViewModel.SetValues(delete.IsSuccess, null, delete.Message);
            }
            catch (Exception)
            {
                _ajaxViewModel.SetValues(false, null, "Failed Message : ");
            }
            return Json(_ajaxViewModel);
        }

        [HttpPost]
        public IActionResult DeleteDetail(int id)
        {
            try
            {
                var endpoint = $"{ApiUrl.BaseUrl}/ARDetail/DeleteRow/{id}";
                var delete = RestAPIHelper<AjaxViewModel>.Submit("", Method.POST, endpoint, Request);
                _ajaxViewModel.SetValues(delete.IsSuccess, null, delete.Message);
            }
            catch (Exception)
            {
                _ajaxViewModel.SetValues(false, null, "Failed Message : ");
            }
            return Json(_ajaxViewModel);
        }
    }
}