﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Iglo.World.DataAccess.Models;
using Iglo.World.Utilities;
using Iglo.World.ViewModels.Ticket;

namespace Iglo.World.Providers
{
    public interface ITicketService : IDataService<Ticket>
    {
        IQueryable<ListTicketViewModel> GetList(string CurrentUser);
        IQueryable<ListTicketViewModel> GetAssignList(string CurrentUser);
        DetailTicketViewModel GetTicket(int ID);
        IQueryable<ListTicketViewModel> GetNewListTicket();
        bool AnyExpiredTicket();
        IQueryable<ListTicketViewModel> GetAllTicket();
        IQueryable<ListTicketViewModel> GetListByCurrentUserAndCategory(string currentUser, TransactionStatusEnum transactionStatus);
    }
    public class TicketProvider : ITicketService
    {
        readonly IgloWorldContext context;
        public TicketProvider(IgloWorldContext context)
        {
            this.context = context;
        }

        public int Add(Ticket entity, string CurrentUser)
        {
            context.SbAdd(entity, CurrentUser);
            context.SaveChanges();
            return entity.Id;
        }

        public void Delete(int Id)
        {
            throw new NotImplementedException();
        }

        public int Edit(Ticket entity, string CurrentUser)
        {
            context.SbEdit(entity, CurrentUser);
            return context.SaveChanges();
        }

        public IQueryable<Ticket> Get()
        {
            throw new NotImplementedException();
        }

        public DataAccess.Models.Ticket Get(int id)
        {
            return context.Ticket.SingleOrDefault(m => m.Id == id);
        }

        public IQueryable<ListTicketViewModel> GetList(string CurrentUser)
        {
            return from tick in context.Ticket
                   join issu in context.Issue on tick.IssueId equals issu.Id
                   join stats in context.TransactionStatus on tick.TransactionStatusId equals stats.Id
                   where tick.CreatedBy == CurrentUser
                   select new ListTicketViewModel
                   {
                       ID = tick.Id,
                       IssueName = issu.Name,
                       Subject = tick.Subject,
                       Description = tick.Description,
                       Status = stats.Name
                   };
        }

        public IQueryable<ListTicketViewModel> GetAssignList(string CurrentUser)
        {
            return from tick in context.Ticket
                   join issu in context.Issue on tick.IssueId equals issu.Id
                   join stats in context.TransactionStatus on tick.TransactionStatusId equals stats.Id
                   where tick.PersonInCharge == CurrentUser && (tick.TransactionStatusId == (int)TransactionStatusEnum.NewTicket || tick.TransactionStatusId == (int)TransactionStatusEnum.OnProgress) 
                         && !tick.ClosedDate.HasValue && !tick.DeletedDate.HasValue 
                   orderby tick.Id
                   select new ListTicketViewModel
                   {
                       ID = tick.Id,
                       IssueName = issu.Name,
                       Subject = tick.Subject,
                       Description = tick.Description,
                       Status = stats.Name
                   };
        }

        public IQueryable<ListTicketViewModel> GetNewListTicket()
        {
            return from tick in context.Ticket
                   join issu in context.Issue on tick.IssueId equals issu.Id
                   join stats in context.TransactionStatus on tick.TransactionStatusId equals stats.Id
                   where tick.PersonInCharge == null && tick.TransactionStatusId == (int)TransactionStatusEnum.NewTicket && !tick.DeletedDate.HasValue
                   select new ListTicketViewModel
                   {
                       ID = tick.Id,
                       IssueName = issu.Name,
                       Subject = tick.Subject,
                       Description = tick.Description,
                       Status = stats.Name
                   };
        }

        public DetailTicketViewModel GetTicket(int ID)
        {
            var contextModel = context.Ticket.SingleOrDefault(m => m.Id == ID);
            var issueContextModel = context.Issue.SingleOrDefault(m => m.Id == contextModel.IssueId);

            var model = new DetailTicketViewModel();
            model.ID = contextModel.Id;
            model.Issue = issueContextModel.Name;
            model.Subject = contextModel.Subject;
            model.Description = contextModel.Description;
            model.OpenTicketDate = contextModel.CreatedDate;
            model.TicketExpireDate = contextModel.ExpiredDate;
            model.CreatedBy = contextModel.CreatedBy;
            model.TransactionStatus = contextModel.TransactionStatusId;
            model.PICName = contextModel.PersonInCharge;

            return model;
        }

        public bool AnyExpiredTicket()
        {
            return context.Ticket.Where(m => !m.DeletedDate.HasValue && m.ExpiredDate < DateTime.Today && !m.ClosedDate.HasValue).Any();
        }

        public IQueryable<ListTicketViewModel> GetAllTicket()
        {
            return from tick in context.Ticket
                   join issu in context.Issue on tick.IssueId equals issu.Id
                   join stats in context.TransactionStatus on tick.TransactionStatusId equals stats.Id
                   where !tick.DeletedDate.HasValue
                   select new ListTicketViewModel
                   {
                       ID = tick.Id,
                       IssueName = issu.Name,
                       Subject = tick.Subject,
                       Description = tick.Description,
                       Status = stats.Name
                   };
        }
        public IQueryable<ListTicketViewModel> GetListByCurrentUserAndCategory(string currentUser, TransactionStatusEnum transactionStatus)
        {
            return from tick in context.Ticket
                   join issu in context.Issue on tick.IssueId equals issu.Id
                   join stats in context.TransactionStatus on tick.TransactionStatusId equals stats.Id
                   where tick.CreatedBy == currentUser && tick.TransactionStatusId == ((int)transactionStatus)
                   select new ListTicketViewModel
                   {
                       ID = tick.Id,
                       IssueName = issu.Name,
                       Subject = tick.Subject,
                       Description = tick.Description,
                       Status = stats.Name
                   };
        }
    }
}
