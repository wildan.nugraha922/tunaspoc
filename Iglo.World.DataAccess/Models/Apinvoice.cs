﻿using System;
using System.Collections.Generic;

namespace Iglo.World.DataAccess.Models
{
    public partial class Apinvoice
    {
        public string VendorId { get; set; }
        public string InvoiceNum { get; set; }
        public int InvoiceLine { get; set; }
        public string Description { get; set; }
        public string TermsCode { get; set; }
        public DateTime ApplyDate { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string PartNum { get; set; }
        public string LineDesc { get; set; }
        public decimal Qty { get; set; }
        public string Pum { get; set; }
        public decimal Cost { get; set; }
        public decimal TotalPrice { get; set; }
        public string CurrencyCode { get; set; }
        public bool Synchronized { get; set; }
        public bool? Paid { get; set; }
        public string EpicorMessage { get; set; }
        public string Flag { get; set; }
    }
}
