﻿using Iglo.World.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Iglo.World.Providers
{
    public interface IArDetailService : IDataService<Ardetail>
    {
        IQueryable<Ardetail> Get(string invoiceNum);
    }
    public class ARDetailProvider : IArDetailService
    {
        readonly TunasPOCContext context;
        public ARDetailProvider(TunasPOCContext context)
        {
            this.context = context;
        }
        public int Add(Ardetail entity, string currentUser)
        {
            context.SbAdd(entity, currentUser);
            context.SaveChanges();
            return entity.ArdetailId;
        }

        public void Delete(int Id)
        {
            var data = Get(Id);
            if (data != null)
            {
                context.Remove(data);
                context.SaveChanges();
            }
        }

        public int Edit(Ardetail entity, string currentUser)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Ardetail> Get()
        {
            return context.Ardetail;
        }

        public Ardetail Get(int id)
        {
            return Get().FirstOrDefault(x => x.ArdetailId.Equals(id));
        }

        public IQueryable<Ardetail> Get(string invoiceNum)
        {
            return Get().Where(m => m.InvoiceNum == invoiceNum);
        }
    }
}
