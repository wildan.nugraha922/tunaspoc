﻿using Iglo.World.DataAccess.Models;
using Iglo.World.Utilities;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Linq;
using System.Reflection;

namespace Iglo.World.Providers
{
    public interface IDataService<T> where T : class
    {
        IQueryable<T> Get();
        T Get(int id);
        int Add(T entity, string currentUser);
        int Edit(T entity, string currentUser);
       
        void Delete(int Id);
    }

    public interface IDataService2<T> where T : class
    {
        IQueryable<T> Get();
        T Get(int id);
    }

    public static class DbContextExtension
    {
        private static void SetValue<TEntity>(TEntity entity, string propName, object value)
        {
            var propertyInfo = entity.GetType()
                .GetProperty(propName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

            if (propertyInfo != null)
                propertyInfo.SetValue(entity, value, null);
        }

        public static EntityEntry<TEntity> SbAdd<TEntity>(this TunasPOCContext context, TEntity entity, string user) where TEntity : class
        {
            SetValue(entity, "CreatedBy", user);
            SetValue(entity, "CreatedDate", DataHelper.GetLocalTimes());

            return context.Set<TEntity>().Add(entity);
        }
        public static EntityEntry<TEntity> SbEdit<TEntity>(this TunasPOCContext context, TEntity entity, string user) where TEntity : class
        {
            SetValue(entity, "UpdatedBy", user);
            SetValue(entity, "UpdatedDate", DataHelper.GetLocalTimes());

            return context.Set<TEntity>().Update(entity);
        }
        public static EntityEntry<TEntity> SbEditProfileExternal<TEntity>(this TunasPOCContext context, TEntity entity, string user) where TEntity : class
        {
            return context.Set<TEntity>().Update(entity);
        }

        
        public static EntityEntry<TEntity> SbDelete<TEntity>(this TunasPOCContext context, TEntity entity) where TEntity : class
        {
            SetValue(entity, "DeletedDate", DataHelper.GetLocalTimes());

            return context.Set<TEntity>().Update(entity);
        }
    }



}
