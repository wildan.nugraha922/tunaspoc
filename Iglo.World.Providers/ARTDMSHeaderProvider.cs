﻿using Iglo.World.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Iglo.World.Providers
{
    public interface IArTDMSHeaderService : IDataService<RpaArheader>
    {
        string Add(RpaArheader entity, string currentUser);
        string Edit(RpaArheader entity, string currentUser);
        void Delete(string invoiceNum);
        RpaArheader Get(string invoiceNum);
        RpaArheader GetDataByInvoiceNumber(string invoiceNumber);
        void AddInvoice(Arinvoice model);
        bool IsDataExistInInvoice(string invoiceNumber);
        int CountHEader();
    }
    public class ARTDMSHeaderProvider : IArTDMSHeaderService
    {
        readonly TunasPOCContext context;
        public ARTDMSHeaderProvider(TunasPOCContext context)
        {
            this.context = context;
        }

        int IDataService<RpaArheader>.Add(RpaArheader entity, string currentUser)
        {
            throw new NotImplementedException();
        }

        public string Add(RpaArheader entity, string currentUser)
        {
            context.SbAdd(entity, currentUser);
            context.SaveChanges();
            return entity.InvoiceNum;
        }

        public void Delete(int Id)
        {
            throw new NotImplementedException();
        }

        public void Delete(string invoiceNum)
        {
            var data = Get(invoiceNum);
            if (data != null)
            {
                context.Remove(data);
                //context.SbDelete(data);
                context.SaveChanges();
            }
        }

        public int Edit(RpaArheader entity, string currentUser)
        {
            throw new NotImplementedException();
        }

        string IArTDMSHeaderService.Edit(RpaArheader entity, string currentUser)
        {
            var data = Get(entity.InvoiceNum);
            if (data != null)
            {
                context.SbEdit(data, currentUser);
                context.SaveChanges();
            }
            return entity.InvoiceNum;
        }

        public IQueryable<RpaArheader> Get()
        {
            return context.RpaArheader;
        }

        public RpaArheader Get(int id)
        {
            throw new NotImplementedException();
        }

        public RpaArheader Get(string invoiceNum)
        {
            return context.RpaArheader.FirstOrDefault(x => x.InvoiceNum.Equals(invoiceNum));
        }

        public RpaArheader GetDataByInvoiceNumber(string invoiceNumber)
        {
            return context.RpaArheader.FirstOrDefault(x => x.InvoiceNum.Equals(invoiceNumber, StringComparison.OrdinalIgnoreCase));

        }

        public void AddInvoice(Arinvoice model)
        {
            this.context.Arinvoice.Add(model);
            this.context.SaveChanges();
        }

        public bool IsDataExistInInvoice(string invoiceNumber)
        {
            return this.context.Arinvoice.Where(x => x.InvoiceNum.Equals(invoiceNumber, StringComparison.OrdinalIgnoreCase)).Any();
        }


        public int CountHEader()
        {
            return this.context.Arheader.Count();
        }
    }
}
