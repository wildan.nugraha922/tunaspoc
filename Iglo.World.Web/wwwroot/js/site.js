﻿const baseurl = 'https://localhost:44317';
window.apiLoginUrl = baseurl + "/Account/Login";
window.changePasswordUrl = baseurl + "/Account/ChangePassword";
window.mainMenuUrl = baseurl + "/";
window.loginUrl = baseurl + "/Login";
window.menuUrl = baseurl + "/Account/GetMenu";