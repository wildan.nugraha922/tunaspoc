﻿using System;

namespace Iglo.World.Utilities
{
    public class DataHelper
    {
        public static DateTime GetLocalTimes()
        {
            DateTime timeUtc = DateTime.UtcNow;
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
            DateTime currentTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, cstZone);
            return currentTime;

        }

        public static DateTime GetLocalDates()
        {
            DateTime timeUtc = DateTime.UtcNow;
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
            DateTime currentTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, cstZone);
            return currentTime.Date;

        }

        public static string GetDateString(DateTime date) {
            if (date == null)
                return "";
            return date.ToString("dd MMM yyyy");
        }
        public static string GetDateString(DateTime? date)
        {
            if (date == null)
                return "";
            DateTime realDate = (DateTime)date;
            return realDate.ToString("dd MMM yyyy");
        }
        public static string GetDatetimeString(DateTime date)
        {
            if (date == null)
                return "";
            return date.ToString("dd MMM yyyy HH:mm:ss");
        }
        public static string GetDatetimeString(DateTime? date)
        {
            if (date == null)
                return "";
            var realDate = (DateTime)date;
            return realDate.ToString("dd MMM yyyy HH:mm:ss");
        }
    }
}
