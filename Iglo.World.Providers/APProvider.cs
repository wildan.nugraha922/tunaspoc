﻿using Iglo.World.DataAccess.Models;
using Iglo.World.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Iglo.World.Providers
{
    public interface IAPService 
    {
        string AddHeader(Apheader entity);
        int AddDetail(Apdetail entity);
        void Delete(string invoiceNumber);
        IQueryable<Apheader> Get();
        IQueryable<Apdetail> GetListDetail(string invoiceNumber);
        Apheader Get(string invoiceNumber);
        bool IsDataExist(string invoiceNumber);
        string GetInvoiceNumber();
        void AddInvoice(Apinvoice model);
        Apheader GetDataByInvoiceNumber(string invoiceNumber);
        bool IsDataExistInInvoice(string invoiceNumber);

    }
    public class APProvider : IAPService
    {
        readonly TunasPOCContext context;
        public APProvider(TunasPOCContext context)
        {
            this.context = context;
        }
        public string AddHeader(Apheader entity)
        {
            context.Apheader.Add(entity);
            context.SaveChanges();
            return entity.InvoiceNum;
        }
        public int AddDetail(Apdetail entity)
        {
            context.Apdetail.Add(entity);
            context.SaveChanges();
            return entity.ApdetailId;
        }
        public void Delete(string invoiceNumber)
        {
            var data = context.Apheader.Where(x => x.InvoiceNum.Equals(invoiceNumber, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            context.Remove(data);
            context.SaveChanges();
        }
        public IQueryable<Apheader> Get() 
        {
            return context.Apheader;
        }
        public IQueryable<Apdetail> GetListDetail(string invoiceNumber)
        {
            return context.Apdetail.Where(x=>x.InvoiceNum.Equals(invoiceNumber,StringComparison.OrdinalIgnoreCase));
        }
        public Apheader Get(string invoiceNumber)
        {
            return context.Apheader.FirstOrDefault(x => x.InvoiceNum.Equals(invoiceNumber,StringComparison.OrdinalIgnoreCase));
        }

        public bool IsDataExist(string invoiceNumber)
        {
            return context.Apheader.Where(x => x.InvoiceNum.Equals(invoiceNumber, StringComparison.OrdinalIgnoreCase)).Any();
        }

        public string GetInvoiceNumber()
        {
            var count = context.Apheader.Count();
            count += context.RpaApheader.Count();
            count+=6;
            var INV = "BILL-POC-" + count.ToString("D3");
            return INV;
            //return $"BILL-POC-00{count}";
        }

        public void AddInvoice(Apinvoice model)
        {
            this.context.Apinvoice.Add(model);
            this.context.SaveChanges();
        }

        public Apheader GetDataByInvoiceNumber(string invoiceNumber)
        {
            return this.context.Apheader.FirstOrDefault(x => x.InvoiceNum.Equals(invoiceNumber, StringComparison.OrdinalIgnoreCase));
        }

        public bool IsDataExistInInvoice(string invoiceNumber)
        {
            return this.context.Apinvoice.Where(x => x.InvoiceNum.Equals(invoiceNumber, StringComparison.OrdinalIgnoreCase)).Any();
        }
    }
}