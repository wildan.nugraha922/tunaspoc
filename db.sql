USE [TunasPOC]
GO
/****** Object:  Table [dbo].[APDetail]    Script Date: 7/22/2020 10:54:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[APDetail](
	[APDetailID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceNum] [nvarchar](50) NOT NULL,
	[PartNum] [nvarchar](50) NOT NULL,
	[PartDescriptions] [nvarchar](1000) NOT NULL,
	[Qty] [decimal](18, 5) NOT NULL,
	[UOM] [nvarchar](6) NOT NULL,
	[Cost] [numeric](18, 3) NOT NULL,
	[TotalCost] [numeric](18, 3) NOT NULL,
 CONSTRAINT [PK_APDetail] PRIMARY KEY CLUSTERED 
(
	[APDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[APHeader]    Script Date: 7/22/2020 10:54:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[APHeader](
	[InvoiceNum] [nvarchar](50) NOT NULL,
	[InvoiceDate] [date] NOT NULL,
	[HeaderDescription] [nvarchar](30) NOT NULL,
	[VendorID] [nvarchar](50) NOT NULL,
	[CurrencyID] [nvarchar](4) NOT NULL,
	[TermsID] [nvarchar](4) NOT NULL,
 CONSTRAINT [PK_APHeader] PRIMARY KEY CLUSTERED 
(
	[InvoiceNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ARDetail]    Script Date: 7/22/2020 10:54:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ARDetail](
	[ARDetailID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceNum] [nvarchar](50) NOT NULL,
	[PartNum] [nvarchar](50) NOT NULL,
	[PartDescriptions] [nvarchar](1000) NOT NULL,
	[Qty] [decimal](18, 5) NOT NULL,
	[UOM] [nvarchar](6) NOT NULL,
	[Cost] [numeric](18, 3) NOT NULL,
	[TotalCost] [numeric](18, 3) NOT NULL,
 CONSTRAINT [PK_ARDetail] PRIMARY KEY CLUSTERED 
(
	[ARDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ARHeader]    Script Date: 7/22/2020 10:54:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ARHeader](
	[InvoiceNum] [nvarchar](50) NOT NULL,
	[InvoiceDate] [date] NOT NULL,
	[HeaderDescription] [nvarchar](30) NOT NULL,
	[CustID] [nvarchar](50) NOT NULL,
	[CurrencyID] [nvarchar](4) NOT NULL,
	[TermsID] [nvarchar](4) NOT NULL,
 CONSTRAINT [PK_ARHeader] PRIMARY KEY CLUSTERED 
(
	[InvoiceNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 7/22/2020 10:54:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](30) NOT NULL,
	[Description] [varchar](500) NULL,
	[IsAdmin] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[DeletedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RPA_APDetail]    Script Date: 7/22/2020 10:54:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RPA_APDetail](
	[APDetailID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceNum] [nvarchar](50) NOT NULL,
	[PartNum] [nvarchar](50) NOT NULL,
	[PartDescriptions] [nvarchar](1000) NOT NULL,
	[Qty] [decimal](18, 5) NOT NULL,
	[UOM] [nvarchar](6) NOT NULL,
	[Cost] [numeric](18, 3) NOT NULL,
	[TotalCost] [numeric](18, 3) NOT NULL,
 CONSTRAINT [PK_RPA_APDetail] PRIMARY KEY CLUSTERED 
(
	[APDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RPA_APHeader]    Script Date: 7/22/2020 10:54:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RPA_APHeader](
	[InvoiceNum] [nvarchar](50) NOT NULL,
	[InvoiceDate] [date] NOT NULL,
	[HeaderDescription] [nvarchar](30) NOT NULL,
	[VendorID] [nvarchar](50) NOT NULL,
	[CurrencyID] [nvarchar](4) NOT NULL,
	[TermsID] [nvarchar](4) NOT NULL,
 CONSTRAINT [PK_RPA_APHeader] PRIMARY KEY CLUSTERED 
(
	[InvoiceNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RPA_ARDetail]    Script Date: 7/22/2020 10:54:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RPA_ARDetail](
	[ARDetailID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceNum] [nvarchar](50) NOT NULL,
	[PartNum] [nvarchar](50) NOT NULL,
	[PartDescriptions] [nvarchar](1000) NOT NULL,
	[Qty] [decimal](18, 5) NOT NULL,
	[UOM] [nvarchar](6) NOT NULL,
	[Cost] [numeric](18, 3) NOT NULL,
	[TotalCost] [numeric](18, 3) NOT NULL,
 CONSTRAINT [PK_RPA_ARDetail] PRIMARY KEY CLUSTERED 
(
	[ARDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RPA_ARHeader]    Script Date: 7/22/2020 10:54:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RPA_ARHeader](
	[InvoiceNum] [nvarchar](50) NOT NULL,
	[InvoiceDate] [date] NOT NULL,
	[HeaderDescription] [nvarchar](30) NOT NULL,
	[CustID] [nvarchar](50) NOT NULL,
	[CurrencyID] [nvarchar](4) NOT NULL,
	[TermsID] [nvarchar](4) NOT NULL,
 CONSTRAINT [PK_RPA_ARHeader] PRIMARY KEY CLUSTERED 
(
	[InvoiceNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Role] ON 

INSERT [dbo].[Role] ([ID], [Name], [Description], [IsAdmin], [IsActive], [CreatedBy], [CreatedDate], [UpdatedDate], [DeletedDate]) VALUES (6, N'Test 2', NULL, 0, 1, N'Wildan', CAST(N'2020-07-22T22:24:17.857' AS DateTime), CAST(N'2020-07-22T22:24:31.633' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[Role] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_ARHeader]    Script Date: 7/22/2020 10:54:17 PM ******/
CREATE NONCLUSTERED INDEX [IX_ARHeader] ON [dbo].[ARHeader]
(
	[InvoiceNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[APDetail]  WITH CHECK ADD  CONSTRAINT [FK_APDetail_APHeader] FOREIGN KEY([InvoiceNum])
REFERENCES [dbo].[APHeader] ([InvoiceNum])
GO
ALTER TABLE [dbo].[APDetail] CHECK CONSTRAINT [FK_APDetail_APHeader]
GO
ALTER TABLE [dbo].[ARDetail]  WITH CHECK ADD  CONSTRAINT [FK_ARDetail_ARDetail] FOREIGN KEY([InvoiceNum])
REFERENCES [dbo].[ARHeader] ([InvoiceNum])
GO
ALTER TABLE [dbo].[ARDetail] CHECK CONSTRAINT [FK_ARDetail_ARDetail]
GO
ALTER TABLE [dbo].[RPA_APDetail]  WITH CHECK ADD  CONSTRAINT [FK_RPA_APDetail_RPA_APHeader] FOREIGN KEY([InvoiceNum])
REFERENCES [dbo].[RPA_APHeader] ([InvoiceNum])
GO
ALTER TABLE [dbo].[RPA_APDetail] CHECK CONSTRAINT [FK_RPA_APDetail_RPA_APHeader]
GO
ALTER TABLE [dbo].[RPA_ARDetail]  WITH CHECK ADD  CONSTRAINT [FK_RPA_ARDetail_RPA_ARHeader] FOREIGN KEY([InvoiceNum])
REFERENCES [dbo].[RPA_ARHeader] ([InvoiceNum])
GO
ALTER TABLE [dbo].[RPA_ARDetail] CHECK CONSTRAINT [FK_RPA_ARDetail_RPA_ARHeader]
GO
USE [master]
GO
ALTER DATABASE [TunasPOC] SET  READ_WRITE 
GO
