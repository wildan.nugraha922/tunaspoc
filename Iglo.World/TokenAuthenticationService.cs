﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Iglo.World.Api
{
    public class TokenRequest
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }
    public interface IAuthenticateService
    {
        bool IsAuthenticated(TokenRequest request, out string token);
    }
    public class TokenAuthenticationService : IAuthenticateService
    {
        public bool IsAuthenticated(TokenRequest request, out string token)
        {
            throw new System.NotImplementedException();
        }
    }
}
