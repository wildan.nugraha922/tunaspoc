﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Iglo.World.DataAccess.Models;
using Iglo.World.Providers;
using Iglo.World.Utilities;
using Iglo.World.ViewModels.Ticket;
using IgloWorld.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Iglo.World.Api.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class TicketController : ControllerBase
    {
        private readonly ITicketService _ticketProvider;
        private readonly ITicketDetailService _ticketDetailProvider;
        private readonly ITicketStatusLogService _ticketStatusLogProvider;
        private readonly IIssueService _issueProvider;
        private IUserService _userProvider;
        private IMapper _mapper;
        private readonly ILogger<TicketController> _log;

        public TicketController(ILogger<TicketController> log, ITicketService ticketProvider, ITicketDetailService ticketDetailProvider, ITicketStatusLogService ticketStatusLogProvider,
            IIssueService issueProvider, IUserService userProvider, IMapper mapper)
        {
            _log = log;
            _ticketProvider = ticketProvider;
            _ticketDetailProvider = ticketDetailProvider;
            _ticketStatusLogProvider = ticketStatusLogProvider;
            _issueProvider = issueProvider;
            _mapper = mapper;
            _userProvider = userProvider;
        }

        [HttpGet]
        [Route("[Action]")]
        public IActionResult GetListIssue()
        {
            var ajaxVm = new AjaxViewModel();
            try
            {
                var list = _issueProvider.GetIssue();

                ajaxVm.SetValues(true, list, "Data has been generated");
                return Ok(ajaxVm);
            }
            catch (Exception e)
            {
                _log.LogInformation(e.Message);
                ajaxVm.SetValues(false, null, "Error when generate data");
                return Ok(ajaxVm);
            }
        }

        [HttpGet]
        [Route("[Action]")]
        public IActionResult GetListNewTicket()
        {
            var ajaxVm = new AjaxViewModel();
            ajaxVm.SetValues(true, null, "Data has been generated");
            try
            {
                var currentUser = HttpContext.User.Identity.Name;

                if (currentUser != AppConstant.HRAdmin)
                    return Ok(ajaxVm);

                var list = _ticketProvider.GetNewListTicket();

                ajaxVm.SetValues(true, list, "Data has been generated");
                return Ok(ajaxVm);
            }
            catch (Exception e)
            {
                _log.LogInformation(e.Message);
                ajaxVm.SetValues(false, null, "Error when generate data");
                return Ok(ajaxVm);
            }
        }

        [HttpPost]
        [Route("[Action]")]
        public IActionResult AssignTicket(AssignTicketViewModel model)
        {
            var ajaxVm = new AjaxViewModel();
            try
            {
                var currentUser = HttpContext.User.Identity.Name;

                var TicketContext = _ticketProvider.Get(model.TicketID);

                TicketContext.PersonInCharge = model.PICName;
                TicketContext.ExpiredDate = DateTime.Today.AddDays(3);

                _ticketProvider.Edit(TicketContext, currentUser);

                ajaxVm.SetValues(true, null, "Data has been generated");
                return Ok(ajaxVm);
            }
            catch (Exception e)
            {
                _log.LogInformation(e.Message);
                ajaxVm.SetValues(false, null, "Error when generate data");
                return Ok(ajaxVm);
            }
        }

        [HttpGet]
        [Route("[Action]")]
        public IActionResult GetAssignListTicket()
        {
            var ajaxVm = new AjaxViewModel();
            try
            {
                var currentUser = HttpContext.User.Identity.Name;
                var list = _ticketProvider.GetAssignList(currentUser);

                ajaxVm.SetValues(true, list, "Data has been generated");
                return Ok(ajaxVm);
            }
            catch (Exception e)
            {
                _log.LogInformation(e.Message);
                ajaxVm.SetValues(false, null, "Error when generate data");
                return Ok(ajaxVm);
            }
        }

        [HttpGet]
        [Route("[Action]")]
        public IActionResult GetAllTicket()
        {
            var ajaxVm = new AjaxViewModel();
            try
            {
                var list = _ticketProvider.GetAllTicket();

                ajaxVm.SetValues(true, list, "Data has been generated");
                return Ok(ajaxVm);
            }
            catch (Exception e)
            {
                _log.LogInformation(e.Message);
                ajaxVm.SetValues(false, null, "Error when generate data");
                return Ok(ajaxVm);
            }
        }

        [HttpGet]
        [Route("[Action]")]
        public IActionResult GetListTicket()
        {
            var ajaxVm = new AjaxViewModel();
            try
            {
                var currentUser = HttpContext.User.Identity.Name;
                var list = _ticketProvider.GetList(currentUser);

                ajaxVm.SetValues(true, list, "Data has been generated");
                return Ok(ajaxVm);
            }
            catch (Exception e)
            {
                _log.LogInformation(e.Message);
                ajaxVm.SetValues(false, null, "Error when generate data");
                return Ok(ajaxVm);
            }
        }

        [HttpGet]
        [Route("[Action]")]
        public IActionResult GetTicket(int ID)
        {
            var ajaxVm = new AjaxViewModel();
            try
            {
                var hasExpiredTicket = _ticketProvider.AnyExpiredTicket();
                var currentUser = HttpContext.User.Identity.Name;

                var model = _ticketProvider.GetTicket(ID);
                var isHRAdmin = _userProvider.isHRAdmin(currentUser);
                model.isHRAdmin = isHRAdmin;

                if (hasExpiredTicket)
                    if (!model.TicketExpireDate.HasValue || model.TicketExpireDate >= DateTime.Today)
                        ajaxVm.SetValues(true, null, "You Have an Expired Ticket Please Solve Expired Ticket First");

                ajaxVm.SetValues(true, model, "Data has been generated");
                return Ok(ajaxVm);
            }
            catch (Exception e)
            {
                _log.LogInformation(e.Message);
                ajaxVm.SetValues(false, null, "Error when generate data");
                return Ok(ajaxVm);
            }
        }

        [HttpGet]
        [Route("[Action]")]
        public IActionResult GetListRepliesTicket(int TicketID)
        {
            var ajaxVm = new AjaxViewModel();
            try
            {
                var model = _ticketDetailProvider.GetReplies(TicketID);

                ajaxVm.SetValues(true, model, "Data has been generated");
                return Ok(ajaxVm);
            }
            catch (Exception e)
            {
                _log.LogInformation(e.Message);
                ajaxVm.SetValues(false, null, "Error when generate data");
                return Ok(ajaxVm);
            }
        }

        [HttpPost]
        [Route("[Action]")]
        public IActionResult AddTicket(AddTicketViewModel viewmodel)
        {
            var ajaxVm = new AjaxViewModel();
            if (viewmodel is null)
            {
                ajaxVm.SetValues(false, null, "Data is null");
                return BadRequest(ajaxVm);
            }
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            try
            {
                var currentUser = HttpContext.User.Identity.Name;
                Ticket model = _mapper.Map<Ticket>(viewmodel);

                model.CreatedBy = currentUser;
                model.TransactionStatusId = (int)TransactionStatusEnum.NewTicket;
                model.Id = _ticketProvider.Add(model, currentUser);

                TicketStatusLog statusModel = new TicketStatusLog();
                statusModel.TicketId = model.Id;
                statusModel.TransactionStatusId = (int)TransactionStatusEnum.NewTicket;
                _ticketStatusLogProvider.Add(statusModel, currentUser);

                ajaxVm.SetValues(true, null, "Data has been generated");
                return Ok(ajaxVm);
            }
            catch (Exception e)
            {
                _log.LogInformation(e.Message);
                ajaxVm.SetValues(false, null, "Error when generate data");
                return Ok(ajaxVm);
            }
        }

        [HttpPost]
        [Route("[Action]")]
        public IActionResult AddComment(AddCommentViewModel viewmodel)
        {
            var ajaxVm = new AjaxViewModel();
            if (viewmodel is null)
            {
                ajaxVm.SetValues(false, null, "Data is null");
                return BadRequest(ajaxVm);
            }
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            try
            {
                var currentUser = HttpContext.User.Identity.Name;
                TicketDetail model = new TicketDetail();
                model.TicketId = viewmodel.IDTicket;
                model.Comment = viewmodel.CommentText;

                _ticketDetailProvider.Add(model, currentUser);

                var ticketVM = _ticketProvider.Get(viewmodel.IDTicket);
                ticketVM.TransactionStatusId = (int)TransactionStatusEnum.OnProgress;
                _ticketProvider.Edit(ticketVM, currentUser);

                var ticketStatusLogModel = _ticketStatusLogProvider.GetTicketStatus(viewmodel.IDTicket).OrderByDescending(m => m.Id).FirstOrDefault();
                if(ticketStatusLogModel.TransactionStatusId != (int)TransactionStatusEnum.OnProgress)
                {
                    var ticketStatusLogVM = new TicketStatusLog();
                    ticketStatusLogVM.TicketId = viewmodel.IDTicket;
                    ticketStatusLogVM.TransactionStatusId = (int)TransactionStatusEnum.OnProgress;

                    _ticketStatusLogProvider.Add(ticketStatusLogVM, currentUser);
                }

                ajaxVm.SetValues(true, null, "Data has been generated");
                return Ok(ajaxVm);
            }
            catch (Exception e)
            {
                _log.LogInformation(e.Message);
                ajaxVm.SetValues(false, null, "Error when generate data");
                return Ok(ajaxVm);
            }
        }

        [HttpGet]
        [Route("[Action]")]
        public IActionResult SolvedTicket(int ID)
        {
            var ajaxVm = new AjaxViewModel();
            try
            {
                var currentUser = HttpContext.User.Identity.Name;

                var model = _ticketProvider.Get(ID);
                model.TransactionStatusId = (int)TransactionStatusEnum.Solved;
                _ticketProvider.Edit(model, currentUser);

                var ticketStatusLogVM = new TicketStatusLog();
                ticketStatusLogVM.TicketId = ID;
                ticketStatusLogVM.TransactionStatusId = (int)TransactionStatusEnum.Solved;

                _ticketStatusLogProvider.Add(ticketStatusLogVM, currentUser);

                ajaxVm.SetValues(true, null, "Data has been generated");
                return Ok(ajaxVm);
            }
            catch (Exception e)
            {
                _log.LogInformation(e.Message);
                ajaxVm.SetValues(false, null, "Error when generate data");
                return Ok(ajaxVm);
            }
        }

        [HttpGet]
        [Route("[Action]")]
        public IActionResult CloseTicket(int ID, bool Action)
        {
            var ajaxVm = new AjaxViewModel();
            try
            {
                var currentUser = HttpContext.User.Identity.Name;
                int transactionStatus;

                if (Action)
                    transactionStatus = (int)TransactionStatusEnum.Close;
                else
                    transactionStatus = (int)TransactionStatusEnum.OnProgress;

                var model = _ticketProvider.Get(ID);
                model.TransactionStatusId = transactionStatus;
                _ticketProvider.Edit(model, currentUser);

                var ticketStatusLogVM = new TicketStatusLog();
                ticketStatusLogVM.TicketId = ID;
                ticketStatusLogVM.TransactionStatusId = transactionStatus;

                _ticketStatusLogProvider.Add(ticketStatusLogVM, currentUser);

                ajaxVm.SetValues(true, null, "Data has been generated");
                return Ok(ajaxVm);
            }
            catch (Exception e)
            {
                _log.LogInformation(e.Message);
                ajaxVm.SetValues(false, null, "Error when generate data");
                return Ok(ajaxVm);
            }
        }

        [HttpGet]
        [Route("[Action]")]
        public IActionResult NeedMoreTime(int ID)
        {
            var ajaxVm = new AjaxViewModel();
            try
            {
                var currentUser = HttpContext.User.Identity.Name;

                var model = _ticketProvider.Get(ID);
                model.ExpiredDate = DateTime.Today.AddDays(1);
                _ticketProvider.Edit(model, currentUser);

                ajaxVm.SetValues(true, null, "Data has been generated");
                return Ok(ajaxVm);
            }
            catch (Exception e)
            {
                _log.LogInformation(e.Message);
                ajaxVm.SetValues(false, null, "Error when generate data");
                return Ok(ajaxVm);
            }
        }

        [HttpGet]
        [Route("[action]")]
        public IActionResult GetListNewTicketCreatedUser() 
        {
            var currentUser = HttpContext.User.Identity.Name;
            var ajaxVm = new AjaxViewModel();
            try
            {
                var list = _ticketProvider.GetListByCurrentUserAndCategory(currentUser, TransactionStatusEnum.NewTicket);

                ajaxVm.SetValues(true, list, "Data has been generated");
                return Ok(ajaxVm);
            }
            catch (Exception e)
            {
                _log.LogInformation(e.Message);
                ajaxVm.SetValues(false, null, "Error when generate data");
                return Ok(ajaxVm);
            }
        }

        [HttpGet]
        [Route("[action]")]
        public IActionResult GetListOnProgressTicketCreatedUser()
        {
            var currentUser = HttpContext.User.Identity.Name;
            var ajaxVm = new AjaxViewModel();
            try
            {
                var list = _ticketProvider.GetListByCurrentUserAndCategory(currentUser, TransactionStatusEnum.OnProgress);

                ajaxVm.SetValues(true, list, "Data has been generated");
                return Ok(ajaxVm);
            }
            catch (Exception e)
            {
                _log.LogInformation(e.Message);
                ajaxVm.SetValues(false, null, "Error when generate data");
                return Ok(ajaxVm);
            }
        }


        [HttpGet]
        [Route("[action]")]
        public IActionResult GetListClosedTicketCreatedUser()
        {
            var currentUser = HttpContext.User.Identity.Name;
            var ajaxVm = new AjaxViewModel();
            try
            {
                var list = _ticketProvider.GetListByCurrentUserAndCategory(currentUser, TransactionStatusEnum.Close);

                ajaxVm.SetValues(true, list, "Data has been generated");
                return Ok(ajaxVm);
            }
            catch (Exception e)
            {
                _log.LogInformation(e.Message);
                ajaxVm.SetValues(false, null, "Error when generate data");
                return Ok(ajaxVm);
            }
        }


    }
}