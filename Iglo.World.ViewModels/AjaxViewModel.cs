﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IgloWorld.ViewModels
{

    public class AjaxViewModel
    {
        public AjaxViewModel()
        {
        }

        public AjaxViewModel(bool isSuccess, object data, string message)
        {
            IsSuccess = isSuccess;
            Data = data;
            Message = message;
        }

        public void SetValues(bool isSuccess, object data, string message)
        {
            IsSuccess = isSuccess;
            Data = data;
            Message = message;
        }

        public bool IsSuccess { get; set; }
        public object Data { get; set; }
        public string Message { get; set; }
    }

    public class AjaxViewModel<T> where T : class
    {
        public AjaxViewModel()
        {
        }

        public AjaxViewModel(bool isSuccess, T data, string message)
        {
            IsSuccess = isSuccess;
            Data = data;
            Message = message;
        }

        public void SetValues(bool isSuccess, T data, string message)
        {
            IsSuccess = isSuccess;
            Data = data;
            Message = message;
        }

        public bool IsSuccess { get; set; }
        public T Data { get; set; }
        public string Message { get; set; }
    }

}
