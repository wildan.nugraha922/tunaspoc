﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Iglo.World.DataAccess.Models
{
    public partial class TunasPOCContext : DbContext
    {
        public TunasPOCContext()
        {
        }

        public TunasPOCContext(DbContextOptions<TunasPOCContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Apdetail> Apdetail { get; set; }
        public virtual DbSet<Apheader> Apheader { get; set; }
        public virtual DbSet<Apinvoice> Apinvoice { get; set; }
        public virtual DbSet<Ardetail> Ardetail { get; set; }
        public virtual DbSet<Arheader> Arheader { get; set; }
        public virtual DbSet<Arinvoice> Arinvoice { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<RpaApdetail> RpaApdetail { get; set; }
        public virtual DbSet<RpaApheader> RpaApheader { get; set; }
        public virtual DbSet<RpaArdetail> RpaArdetail { get; set; }
        public virtual DbSet<RpaArheader> RpaArheader { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=.;Initial Catalog=TunasPOC;Persist Security Info=True;User ID=sa;Password=P@ssw0rd");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Apdetail>(entity =>
            {
                entity.ToTable("APDetail");

                entity.Property(e => e.ApdetailId).HasColumnName("APDetailID");

                entity.Property(e => e.Cost).HasColumnType("numeric(18, 3)");

                entity.Property(e => e.InvoiceNum)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PartDescriptions)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.PartNum)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Qty).HasColumnType("decimal(18, 5)");

                entity.Property(e => e.TotalCost).HasColumnType("numeric(18, 3)");

                entity.Property(e => e.Uom)
                    .IsRequired()
                    .HasColumnName("UOM")
                    .HasMaxLength(6);

                entity.HasOne(d => d.InvoiceNumNavigation)
                    .WithMany(p => p.Apdetail)
                    .HasForeignKey(d => d.InvoiceNum)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_APDetail_APHeader");
            });

            modelBuilder.Entity<Apheader>(entity =>
            {
                entity.HasKey(e => e.InvoiceNum);

                entity.ToTable("APHeader");

                entity.Property(e => e.InvoiceNum)
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.CurrencyId)
                    .IsRequired()
                    .HasColumnName("CurrencyID")
                    .HasMaxLength(4);

                entity.Property(e => e.HeaderDescription)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.InvoiceDate).HasColumnType("date");

                entity.Property(e => e.TermsId)
                    .IsRequired()
                    .HasColumnName("TermsID")
                    .HasMaxLength(4);

                entity.Property(e => e.VendorId)
                    .IsRequired()
                    .HasColumnName("VendorID")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Apinvoice>(entity =>
            {
                entity.HasKey(e => new { e.VendorId, e.InvoiceNum, e.InvoiceLine });

                entity.ToTable("APInvoice");

                entity.Property(e => e.VendorId)
                    .HasColumnName("VendorID")
                    .HasMaxLength(50);

                entity.Property(e => e.InvoiceNum).HasMaxLength(50);

                entity.Property(e => e.ApplyDate).HasColumnType("date");

                entity.Property(e => e.Cost).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.CurrencyCode)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.EpicorMessage)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Flag)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceDate).HasColumnType("date");

                entity.Property(e => e.LineDesc)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Paid).HasDefaultValueSql("((0))");

                entity.Property(e => e.PartNum)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Pum)
                    .IsRequired()
                    .HasColumnName("PUM")
                    .HasMaxLength(10);

                entity.Property(e => e.Qty).HasColumnType("decimal(18, 5)");

                entity.Property(e => e.TermsCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.TotalPrice).HasColumnType("decimal(18, 3)");
            });

            modelBuilder.Entity<Ardetail>(entity =>
            {
                entity.ToTable("ARDetail");

                entity.Property(e => e.ArdetailId).HasColumnName("ARDetailID");

                entity.Property(e => e.Cost).HasColumnType("numeric(18, 3)");

                entity.Property(e => e.InvoiceNum)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PartDescriptions)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.PartNum)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Qty).HasColumnType("decimal(18, 5)");

                entity.Property(e => e.TotalCost).HasColumnType("numeric(18, 3)");

                entity.Property(e => e.Uom)
                    .IsRequired()
                    .HasColumnName("UOM")
                    .HasMaxLength(6);

                entity.HasOne(d => d.InvoiceNumNavigation)
                    .WithMany(p => p.Ardetail)
                    .HasForeignKey(d => d.InvoiceNum)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ARDetail_ARDetail");
            });

            modelBuilder.Entity<Arheader>(entity =>
            {
                entity.HasKey(e => e.InvoiceNum);

                entity.ToTable("ARHeader");

                entity.HasIndex(e => e.InvoiceNum)
                    .HasName("IX_ARHeader");

                entity.Property(e => e.InvoiceNum)
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.CurrencyId)
                    .IsRequired()
                    .HasColumnName("CurrencyID")
                    .HasMaxLength(4);

                entity.Property(e => e.CustId)
                    .IsRequired()
                    .HasColumnName("CustID")
                    .HasMaxLength(50);

                entity.Property(e => e.HeaderDescription)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.InvoiceDate).HasColumnType("date");

                entity.Property(e => e.TermsId)
                    .IsRequired()
                    .HasColumnName("TermsID")
                    .HasMaxLength(4);
            });

            modelBuilder.Entity<Arinvoice>(entity =>
            {
                entity.HasKey(e => new { e.CustId, e.InvoiceNum, e.InvoiceLine });

                entity.ToTable("ARInvoice");

                entity.Property(e => e.CustId)
                    .HasColumnName("CustID")
                    .HasMaxLength(50);

                entity.Property(e => e.InvoiceNum).HasMaxLength(50);

                entity.Property(e => e.ApplyDate).HasColumnType("date");

                entity.Property(e => e.Cost).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.CurrencyCode)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.EpicorInvoiceNum).HasMaxLength(50);

                entity.Property(e => e.EpicorMessage)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Flag)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceDate).HasColumnType("date");

                entity.Property(e => e.LineDesc)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.PartNum)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Pum)
                    .IsRequired()
                    .HasColumnName("PUM")
                    .HasMaxLength(10);

                entity.Property(e => e.Qty).HasColumnType("decimal(18, 5)");

                entity.Property(e => e.TermsCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.TotalPrice).HasColumnType("decimal(18, 3)");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DeletedDate).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<RpaApdetail>(entity =>
            {
                entity.HasKey(e => e.ApdetailId);

                entity.ToTable("RPA_APDetail");

                entity.Property(e => e.ApdetailId).HasColumnName("APDetailID");

                entity.Property(e => e.Cost).HasColumnType("numeric(18, 3)");

                entity.Property(e => e.InvoiceNum)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PartDescriptions)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.PartNum)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Qty).HasColumnType("decimal(18, 5)");

                entity.Property(e => e.TotalCost).HasColumnType("numeric(18, 3)");

                entity.Property(e => e.Uom)
                    .IsRequired()
                    .HasColumnName("UOM")
                    .HasMaxLength(6);

                entity.HasOne(d => d.InvoiceNumNavigation)
                    .WithMany(p => p.RpaApdetail)
                    .HasForeignKey(d => d.InvoiceNum)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RPA_APDetail_RPA_APHeader");
            });

            modelBuilder.Entity<RpaApheader>(entity =>
            {
                entity.HasKey(e => e.InvoiceNum);

                entity.ToTable("RPA_APHeader");

                entity.Property(e => e.InvoiceNum)
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.CurrencyId)
                    .IsRequired()
                    .HasColumnName("CurrencyID")
                    .HasMaxLength(4);

                entity.Property(e => e.HeaderDescription)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.InvoiceDate).HasColumnType("date");

                entity.Property(e => e.TermsId)
                    .IsRequired()
                    .HasColumnName("TermsID")
                    .HasMaxLength(4);

                entity.Property(e => e.VendorId)
                    .IsRequired()
                    .HasColumnName("VendorID")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<RpaArdetail>(entity =>
            {
                entity.HasKey(e => e.ArdetailId);

                entity.ToTable("RPA_ARDetail");

                entity.Property(e => e.ArdetailId).HasColumnName("ARDetailID");

                entity.Property(e => e.Cost).HasColumnType("numeric(18, 3)");

                entity.Property(e => e.InvoiceNum)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PartDescriptions)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.PartNum)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Qty).HasColumnType("decimal(18, 5)");

                entity.Property(e => e.TotalCost).HasColumnType("numeric(18, 3)");

                entity.Property(e => e.Uom)
                    .IsRequired()
                    .HasColumnName("UOM")
                    .HasMaxLength(6);

                entity.HasOne(d => d.InvoiceNumNavigation)
                    .WithMany(p => p.RpaArdetail)
                    .HasForeignKey(d => d.InvoiceNum)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RPA_ARDetail_RPA_ARHeader");
            });

            modelBuilder.Entity<RpaArheader>(entity =>
            {
                entity.HasKey(e => e.InvoiceNum);

                entity.ToTable("RPA_ARHeader");

                entity.Property(e => e.InvoiceNum)
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.CurrencyId)
                    .IsRequired()
                    .HasColumnName("CurrencyID")
                    .HasMaxLength(4);

                entity.Property(e => e.CustId)
                    .IsRequired()
                    .HasColumnName("CustID")
                    .HasMaxLength(50);

                entity.Property(e => e.HeaderDescription)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.InvoiceDate).HasColumnType("date");

                entity.Property(e => e.TermsId)
                    .IsRequired()
                    .HasColumnName("TermsID")
                    .HasMaxLength(4);
            });
        }
    }
}
