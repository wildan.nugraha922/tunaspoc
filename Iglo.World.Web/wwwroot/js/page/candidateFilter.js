﻿$('.btnPrimary').on('click', function () {

    if ($(this).prop("checked") == true) {
        $(`.btnPrimary`).attr("disabled", true);
        setData(this.id);
    } else {
        enable(this.id);
        $(`.btnPrimary`).removeAttr("disabled");
    }

})
$('.btnDuplicate').on('click', function () {
    if ($(this).prop("checked") == true) {
        setData(this.id);
    } else {
        enable(this.id);
    }
})
$('.btnOther').on('click', function () {
    if ($(this).prop("checked") == true) {
        setData(this.id);
    } else {
        enable(this.id);
    }
})

function setData(value) {
    const arrTemp = value.split('-');
    const type = arrTemp[0];
    const id = arrTemp[1];
    if (type === 'P') {
        $(`#P-${id}`).removeAttr("disabled");
        $(`#O-${id}`).attr("disabled", true);
        $(`#D-${id}`).attr("disabled", true);
    } else if (type === 'D') {
        $(`#P-${id}`).attr("disabled", true);
        $(`#D-${id}`).removeAttr("disabled");
        $(`#O-${id}`).attr("disabled", true);
    } else if (type === 'O') {
        $(`#P-${id}`).attr("disabled", true);
        $(`#D-${id}`).attr("disabled", true);
        $(`#O-${id}`).removeAttr("disabled");
    }
}

function enable(value) {
    const arrTemp = value.split('-');
    const id = arrTemp[1];

    if ($('input[class="btnPrimary"]:checked').length === 0)
        $(`#P-${id}`).removeAttr("disabled");

    $(`#O-${id}`).removeAttr("disabled");
    $(`#D-${id}`).removeAttr("disabled");
}

function checkIsInputChecked(id) {
    return $(`input[id=${id}]:checked`).length > 0;
}

function generateSubmitData() {
    let result = [];
    const data = getAllData();
    for (var i = 0; i < data.length; i++) {
        let currentId = getId(data[i].id);
        //check primary            
        let isPrimary = checkIsInputChecked(`P-${currentId}`);
        if (isPrimary)
            result.push({ id: currentId, status: 'P' });
        //check duplicate
        let isDuplicate = checkIsInputChecked(`D-${currentId}`);
        if (isDuplicate)
            result.push({ id: currentId, status: 'D' });
        //check other data
        let isOtherData = checkIsInputChecked(`O-${currentId}`);
        if (isOtherData)
            result.push({ id: currentId, status: 'O' });
    }
    return result;
}

function getId(value) {
    return value.split('-')[1];
}

function getAllData() {
    return $('.btnPrimary');
}

function onSubmit() {
    const result = generateSubmitData();

    const parentId = $('#Candidate_Id').val();

    if (checkIsExistPrimary(result)) {
        result.push({ id: parentId, status: 'D' });
    } else {
        result.push({ id: parentId, status: 'P' });
    }

    if (result.length === getAllData().length + 1) {
        submitData(JSON.stringify(result)).then(function (response) {
            Starbridges.setEvent(event);
            Starbridges.refreshGrid();
            if ($("#grid-registered").length) {
                Starbridges.refreshGrid("grid-registered");
            }
            if ($("#grid-unregistered").length) {
                Starbridges.refreshGrid("grid-unregistered");
            }

            swal("Information", "Data has been save", "success");
            $("#btnClose").click();
        }).catch(function (err) {
            swal("Information", err, "error");
        });
    } else {
        swal("Information", "Not all data checked", "error");
        return;
    }
}

function submitData(jsonString) {
    const url = '/CandidateFilter/Submit';
    return $.ajax({
        type: "POST",
        url: url,
        data: jsonString,
        contentType: 'application/json'
    });
}

function checkIsExistPrimary(list) {
    return list.filter(x => x.status === 'P').length > 0;
}