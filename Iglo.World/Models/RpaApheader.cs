﻿using System;
using System.Collections.Generic;

namespace Iglo.World.Api.Models
{
    public partial class RpaApheader
    {
        public RpaApheader()
        {
            RpaApdetail = new HashSet<RpaApdetail>();
        }

        public string InvoiceNum { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string HeaderDescription { get; set; }
        public string VendorId { get; set; }
        public string CurrencyId { get; set; }
        public string TermsId { get; set; }

        public virtual ICollection<RpaApdetail> RpaApdetail { get; set; }
    }
}
