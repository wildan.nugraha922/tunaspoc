﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using RestSharp;

namespace Iglo.World.Utilities
{
    public static class ApiUrl
    {        
        public static string BaseUrl => $"{DataConfiguration.Configuration.AppConfiguration.ApiUrl}";

    }
    public class DataConfiguration
    {
        public static Configuration Configuration
        {
            get
            {
                string json = System.IO.File.ReadAllText("appsettings.json");
                var data = JsonConvert.DeserializeObject<Configuration>(json);
                return data;
            }
        }
    }
    public class Configuration
    {
        public AppConfiguration AppConfiguration { get; set; }
        public SmtpSettings SmtpSettings { get; set; }
    }

    public class SmtpSettings 
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class AppConfiguration
    {
        public string ApiUrl { get; set; }
        public string ForgotPasswordUrl { get; set; }
        public string SuperAdmin { get; set; }
        public string ShareFolderAddress { get; set; }
        public string ShareFolderCVAddress { get; set; }

    }
    public class RestAPIHelper<T>
    {
        public static T Submit(string jsonBody, Method httpMethod, string endpoint)
        {
            var requests = new RestRequest(httpMethod);
            requests.AddHeader("Content-Type", "application/json");
            requests.Timeout = 300000;
            if (!string.IsNullOrEmpty(jsonBody))
            {
                requests.AddParameter("application/json", jsonBody, ParameterType.RequestBody);
            }

            var client = new RestClient(endpoint);
            IRestResponse response = client.Execute(requests);
            var result = JsonConvert.DeserializeObject<T>(response.Content);
            return result;
        }
        public static T Submit(string jsonBody, Method httpMethod, string endpoint, HttpRequest httpRequest)
        {

            var requests = new RestRequest(httpMethod);
            requests.AddHeader("Content-Type", "application/json");
            requests.Timeout = 300000;
            string token = null;
            var cookie = httpRequest.Cookies["Auth"];
            token = cookie;
                
            if (!string.IsNullOrEmpty(token))
            {
                requests.AddHeader("Authorization", $"Bearer {token}");
            }

            if (!string.IsNullOrEmpty(jsonBody))
            {
                requests.AddParameter("application/json", jsonBody, ParameterType.RequestBody);
            }
            var client = new RestClient(endpoint);
            IRestResponse response = client.Execute(requests);

            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw new System.Exception("UnAuthorize");
            }            
            var result = JsonConvert.DeserializeObject<T>(response.Content);

            return result;
        }

    }
}
