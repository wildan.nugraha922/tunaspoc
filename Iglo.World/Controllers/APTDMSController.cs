﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Iglo.World.DataAccess.Models;
using Iglo.World.Providers;
using Iglo.World.Utilities;
using Iglo.World.ViewModels;
using IgloWorld.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.TagHelpers;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;

namespace Iglo.World.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class APTDMSController : Controller
    {
        private IAPTDMSService _apService;
        private IMapper _mapper;
        private AjaxViewModel _ajaxViewModel;
        public APTDMSController(IAPTDMSService apService, IMapper mapper)
        {
            _apService = apService;
            _mapper = mapper;
            _ajaxViewModel = new AjaxViewModel();
        }


        [HttpPost]
        [Route("[action]")]
        public IActionResult GetList([FromBody]CustomDataSourceRequest request)
        {
            try
            {
                var data = _apService.Get().ToList();
                var mappedData = new List<ApHeaderViewModel>();

                foreach (var item in data)
                {
                    mappedData.Add(new ApHeaderViewModel { 
                        CurrencyId = item.CurrencyId, 
                        InvoiceDate = item.InvoiceDate, 
                        InvoiceNum = item.InvoiceNum, 
                        TermsId = item.TermsId, 
                        VendorId = item.VendorId, 
                        HeaderDescription = item.HeaderDescription, 
                        InvoiceDateString = DataHelper.GetDateString(item.InvoiceDate) 
                    });
                }
                DataSourceRequest req = GridUtilities.ConvertToKendoFromCustomRequest(request);
                var filter = mappedData.ToDataSourceResult(req);
                return Ok(filter);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult GetListDetail([FromBody]CustomDataSourceRequest request, string invoiceNumber)
        {
            try
            {
                var dataRole = _apService.GetListDetail(invoiceNumber).ToList();
                DataSourceRequest req = GridUtilities.ConvertToKendoFromCustomRequest(request);
                var filter = dataRole.ToDataSourceResult(req);
                return Ok(filter);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult Submit([FromBody] ApHeaderViewModel viewModel)
        {
            string invoiceNumber = "";
            if (viewModel is null)
            {
                _ajaxViewModel.SetValues(false, null, "Data is null");
                return BadRequest(_ajaxViewModel);
            }
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var isDataExist = _apService.IsDataExist(viewModel.InvoiceNum);
            if (!isDataExist)
            {
                var model = _mapper.Map<RpaApheader>(viewModel);
                invoiceNumber = _apService.AddHeader(model);
            }
            _ajaxViewModel.SetValues(true, invoiceNumber, "Saved");
            return Ok(_ajaxViewModel);
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult SubmitDetail([FromBody] ApDetailViewModel viewModel)
        {
            if (viewModel is null)
            {
                _ajaxViewModel.SetValues(false, null, "Data is null");
                return BadRequest(_ajaxViewModel);
            }
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var isDataExistInInvoice = _apService.IsDataExistInInvoice(viewModel.InvoiceNum);
            if (isDataExistInInvoice) {
                _ajaxViewModel.SetValues(false, null, "Data Already exists in invoice");
                return Ok(_ajaxViewModel);
            }

            var isDataExist = _apService.IsDataExist(viewModel.InvoiceNum);
            if (isDataExist)
            {
                var model = _mapper.Map<RpaApdetail>(viewModel);
                _apService.AddDetail(model);
                var dataHeader = _apService.GetDataByInvoiceNumber(model.InvoiceNum);
                var invoice = new Apinvoice { 
                    InvoiceNum = model.InvoiceNum,
                    CurrencyCode = dataHeader.CurrencyId,
                    TermsCode = dataHeader.TermsId,
                    Description = dataHeader.HeaderDescription,
                    VendorId = dataHeader.VendorId,
                    InvoiceDate = DateTime.Now,
                    ApplyDate = DateTime.Now,
                    Flag = "NON RPA",
                    Synchronized = false,
                    Paid = false,
                    EpicorMessage = "",                      
                    InvoiceLine = model.InvoiceLine.HasValue ? model.InvoiceLine.Value : 1,
                    PartNum = model.PartNum,
                    LineDesc = model.PartDescriptions,
                    Qty = model.Qty,
                    Pum = model.Uom,
                    Cost = model.Cost,
                    TotalPrice = model.TotalCost
                };
                //_apService.AddInvoice(invoice);
            }
            _ajaxViewModel.SetValues(true, null, "Saved");
            return Ok(_ajaxViewModel);
        }

        [HttpPost]
        [Route("[action]/{Id}")]
        public IActionResult DeleteRow(string id)
        {
            
            try
            {
                var userExist = _apService.Get(id);
                if (userExist != null)
                {
                    _apService.Delete(id);
                    _ajaxViewModel.SetValues(true, "", "Success! Data has been deleted");

                }
                else
                {
                    _ajaxViewModel.SetValues(false, "", "Failed to Delete");
                }

            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
            return Ok(_ajaxViewModel);
        }
        [HttpGet]
        [Route("[action]")]
        public IActionResult GetInvoiceNumber() 
        {
            string invoiceNumber = _apService.GetInvoiceNumber();
            _ajaxViewModel.SetValues(true, invoiceNumber,"berhasil");
            return Ok(_ajaxViewModel);        
        }
    }
}