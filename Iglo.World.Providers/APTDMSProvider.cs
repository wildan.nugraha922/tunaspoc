﻿using Iglo.World.DataAccess.Models;
using Iglo.World.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Iglo.World.Providers
{
    public interface IAPTDMSService 
    {
        string AddHeader(RpaApheader entity);
        int AddDetail(RpaApdetail entity);
        void Delete(string invoiceNumber);
        IQueryable<RpaApheader> Get();
        IQueryable<RpaApdetail> GetListDetail(string invoiceNumber);
        RpaApheader Get(string invoiceNumber);
        bool IsDataExist(string invoiceNumber);
        string GetInvoiceNumber();
        void AddInvoice(Apinvoice model);
        RpaApheader GetDataByInvoiceNumber(string invoiceNumber);
        bool IsDataExistInInvoice(string invoiceNumber);

    }
    public class APTDMSProvider : IAPTDMSService
    {
        readonly TunasPOCContext context;
        public APTDMSProvider(TunasPOCContext context)
        {
            this.context = context;
        }
        public string AddHeader(RpaApheader entity)
        {
            context.RpaApheader.Add(entity);
            context.SaveChanges();
            return entity.InvoiceNum;
        }
        public int AddDetail(RpaApdetail entity)
        {
            context.RpaApdetail.Add(entity);
            context.SaveChanges();
            return entity.ApdetailId;
        }
        public void Delete(string invoiceNumber)
        {
            var data = context.RpaApheader.Where(x => x.InvoiceNum.Equals(invoiceNumber, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            context.Remove(data);
            context.SaveChanges();
        }
        public IQueryable<RpaApheader> Get() 
        {
            return context.RpaApheader;
        }
        public IQueryable<RpaApdetail> GetListDetail(string invoiceNumber)
        {
            return context.RpaApdetail.Where(x=>x.InvoiceNum.Equals(invoiceNumber,StringComparison.OrdinalIgnoreCase));
        }
        public RpaApheader Get(string invoiceNumber)
        {
            return context.RpaApheader.FirstOrDefault(x => x.InvoiceNum.Equals(invoiceNumber,StringComparison.OrdinalIgnoreCase));
        }

        public bool IsDataExist(string invoiceNumber)
        {
            return context.RpaApheader.Where(x => x.InvoiceNum.Equals(invoiceNumber, StringComparison.OrdinalIgnoreCase)).Any();
        }

        public string GetInvoiceNumber()
        {
            var count = context.Apheader.Count();
            count += context.RpaApheader.Count();
            count += 6;
            var INV = "BILL-POC-" + count.ToString("D3");
            return INV;

            //var count = context.Apheader.Count();
            //count += context.RpaApheader.Count();
            //count += 5;
            //return $"BILL-POC-00{count}";
        }

        public void AddInvoice(Apinvoice model)
        {
            this.context.Apinvoice.Add(model);
            this.context.SaveChanges();
        }

        public RpaApheader GetDataByInvoiceNumber(string invoiceNumber)
        {
            return this.context.RpaApheader.FirstOrDefault(x => x.InvoiceNum.Equals(invoiceNumber, StringComparison.OrdinalIgnoreCase));
        }

        public bool IsDataExistInInvoice(string invoiceNumber)
        {
            return this.context.Apinvoice.Where(x => x.InvoiceNum.Equals(invoiceNumber, StringComparison.OrdinalIgnoreCase)).Any();
        }
    }
}