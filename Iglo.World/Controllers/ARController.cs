﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Iglo.World.DataAccess.Models;
using Iglo.World.Providers;
using Iglo.World.Utilities;
using Iglo.World.ViewModels;
using IgloWorld.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Iglo.World.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ARController : Controller
    {
        private IArHeaderService _arHeaderService;
        private IArDetailService _arDetailService;
        private IMapper _mapper;
        private AjaxViewModel _ajaxViewModel;
        public ARController(IArHeaderService arService, IArDetailService arDetailService, IMapper mapper)
        {
            _arHeaderService = arService;
            _arDetailService = arDetailService;
            _mapper = mapper;
            _ajaxViewModel = new AjaxViewModel();
        }

        [HttpGet]
        [Route("[action]/{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var role = _arHeaderService.Get(id);
                var vm = _mapper.Map<ArheaderViewModel>(role);
                return Ok(vm);
            }

            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult GetList([FromBody]CustomDataSourceRequest request)
        {
            try
            {
                var dataRole = _arHeaderService.Get().ToList();
                DataSourceRequest req = GridUtilities.ConvertToKendoFromCustomRequest(request);
                var filter = dataRole.ToDataSourceResult(req);
                return Ok(filter);
            }

            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult Submit([FromBody] ArheaderViewModel viewModel)
        {
            var currentUser = "Wildan";
            var id = "";
            if (viewModel is null)
            {
                _ajaxViewModel.SetValues(false, null, "Data is null");
                return BadRequest(_ajaxViewModel);
            }
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (string.IsNullOrEmpty(viewModel.InvoiceNum))
            {
                var countData = _arHeaderService.Get().Count();
                countData += _arHeaderService.CountRPAHEader();
                countData += 11;
                viewModel.InvoiceNum = "INV-POC-" + countData;
                if (!_arHeaderService.Get().Any(m => m.InvoiceNum == viewModel.InvoiceNum))
                {
                    var model = _mapper.Map<Arheader>(viewModel);
                    id = _arHeaderService.Add(model, currentUser);
                }
                else
                {
                    var existingRole = _arHeaderService.Get(viewModel.InvoiceNum);
                    _mapper.Map(viewModel, existingRole);
                    _arHeaderService.Edit(existingRole, currentUser);
                }
                _ajaxViewModel.SetValues(true, null, id);
            }
            else {
                var existingRole = _arHeaderService.Get(viewModel.InvoiceNum);
                _mapper.Map(viewModel, existingRole);
                _arHeaderService.Edit(existingRole, currentUser);

                _ajaxViewModel.SetValues(true, null, id);
            }

            return Ok(_ajaxViewModel);
        }

        [HttpPost]
        [Route("[action]/{Id}")]
        public IActionResult DeleteRow(string id)
        {
            try
            {
                if (!_arDetailService.Get(id).Any())
                {
                    var dataExist = _arHeaderService.Get(id);
                    if (dataExist != null)
                    {
                        _arHeaderService.Delete(id);
                        _ajaxViewModel.SetValues(true, "", "Success! Data has been deleted");

                    }
                    else
                    {
                        _ajaxViewModel.SetValues(false, "", "Failed to Delete");
                    }
                }
                else {
                    _ajaxViewModel.SetValues(false, "", "Can't Delete This");
                }
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
            return Ok(_ajaxViewModel);
        }
    }
}