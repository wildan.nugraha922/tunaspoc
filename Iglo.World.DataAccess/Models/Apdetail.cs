﻿using System;
using System.Collections.Generic;

namespace Iglo.World.DataAccess.Models
{
    public partial class Apdetail
    {
        public int ApdetailId { get; set; }
        public string InvoiceNum { get; set; }
        public int? InvoiceLine { get; set; }
        public string PartNum { get; set; }
        public string PartDescriptions { get; set; }
        public decimal Qty { get; set; }
        public string Uom { get; set; }
        public decimal Cost { get; set; }
        public decimal TotalCost { get; set; }

        public virtual Apheader InvoiceNumNavigation { get; set; }
    }
}
