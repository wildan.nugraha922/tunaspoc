﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Iglo.World.ViewModels
{
    public class ApDetailViewModel
    {
        public int ApdetailId { get; set; }
        [StringLength(50, ErrorMessage = "Maximum 50 characters")]
        public string InvoiceNum { get; set; }
        [StringLength(50, ErrorMessage = "Maximum 50 characters")]
        public string PartNum { get; set; }
        [StringLength(1000, ErrorMessage = "Maximum 1000 characters")]
        public string PartDescriptions { get; set; }
        public decimal Qty { get; set; }
        [StringLength(6, ErrorMessage = "Maximum 6 characters")]
        public string Uom { get; set; }
        public decimal Cost { get; set; }
        public decimal TotalCost { get; set; }
        public int? InvoiceLine { get; set; }

    }
}
