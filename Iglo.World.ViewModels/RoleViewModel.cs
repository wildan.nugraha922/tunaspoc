﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace Iglo.World.ViewModels
{

    public class RoleViewModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(50, ErrorMessage = "Maximum 50 characters")]
        [RegularExpression(@"^[0-9a-zA-Z''-'\s]{1,40}$", ErrorMessage = "Special characters are not  allowed.")]
        public string Name { get; set; }
        public string Description { get; set; }

    }
}
